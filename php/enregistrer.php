<?php

	include ("fonctions.php");

    $nom = htmlentities($_POST['nom']);

	$score = htmlentities($_POST['score']);

    $nom = strtolower($nom);
    
    $req = $db -> prepare("SELECT * FROM quiz_classement WHERE nom = :nom");
    $req -> execute(array("nom"=>$nom));

    $trouve = 0;
    $ancien = 0;
    
    while($donnees = $req -> fetch()){
        if(strtolower($donnees['nom']) == $nom){
            $trouve = 1;
            $ancien = $donnees['score'];
        }
    }
    
    if(!$trouve){
		$req = $db -> prepare("INSERT INTO quiz_classement(nom, score,date) VALUES(:nom,:score,NOW())");
		$req -> execute(array("nom"=>$nom, "score"=>$score)); 
        echo 'Nouvel utilisateur';
    }
    else{
        if($ancien < $score){
            $req = $db -> prepare("UPDATE quiz_classement SET score = :score, date = NOW() WHERE nom = :nom");
            $req -> execute(array("nom"=>$nom, "score"=>$score));   
            echo 'Record battu';
        }    
        else{
            echo 'Des efforts';
        }
    }



?>