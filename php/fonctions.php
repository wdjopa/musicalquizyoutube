<?php

	try{
		$pdo_options [PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		$db = new PDO("mysql:host=localhost;dbname=tchatat1_games","root", "", $pdo_options);
		$db->exec("set names utf8mb4");

		//$db = new PDO("mysql:host=djopafrtpqquiz.mysql.db;dbname=djopafrtpqquiz","djopafrtpqquiz", "passwordBDDOVH2019", $pdo_options);
	}
	catch(Exception $e){
		die("Erreur: ".$e->getMessage());
	}
    session_start();

	function c($str){
		$chaine = "";
		$carac="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789àâéêèëîïôöçù',!?.;:=+-()/@%$ *";
		for($i=0;$i<strlen($str);$i++){
			if(strpos($carac,$str[$i])>=0){
				$chaine.=$carac[(strpos($carac,$str[$i]) + 1) % strlen($carac)];
			}
			else{
				$chaine.=$str[$i];	
			}
		}
		return $chaine;
	}

	function questions(){
		global $db;
		$rep = $db -> query("SELECT * FROM quiz_questions ORDER BY RAND() LIMIT 30");
		$a=array();
		while($q = $rep -> fetch()){
			$question = array(
				"question"=>c($q["question"]),
				"reponse"=>c($q["reponse"]),
				"propositions"=>c($q["proposition1"].",".$q["proposition2"].",".$q["proposition3"].",".$q["reponse"]),
				"musique"=>c($q["youtube"]),
				"debut"=>c($q["debut"]),
				"commentaire"=>"",
				"images"=>"images"
			);
			$a[] = $question;
		} 
		return $a;
	}
	function questionsSimple(){
		global $db;
		$rep = $db -> query("SELECT * FROM quiz_questions ORDER BY RAND() LIMIT 30");
		$a=array();
		while($q = $rep -> fetch()){
			$question = array(
				"question"=>($q["question"]),
				"reponse"=>($q["reponse"]),
				"propositions"=>($q["proposition1"].",".$q["proposition2"].",".$q["proposition3"].",".$q["reponse"]),
				"musique"=>($q["youtube"]),
				"debut"=>($q["debut"]),
				"commentaire"=>"",
				"images"=>"images"
			);
			$a[] = $question;
		} 
		return $a;
	}

?>