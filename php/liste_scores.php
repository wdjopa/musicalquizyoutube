<?php
	

	include ("fonctions.php");

	if(isset($_GET['type'])){

		$req = $db -> query("SELECT * FROM quiz_classement ORDER BY score DESC LIMIT 1");
		echo $req->fetch()['score'];
	}
	else{
		$a = 0;
		$b = 1;
		$req = $db -> query("SELECT nom, score, (SELECT avatar FROM quiz_users WHERE quiz_users.username=nom) AS avatar FROM quiz_classement WHERE quiz_classement.nom NOT IN (SELECT quiz_users.username FROM quiz_users WHERE email LIKE '%@tchatat.fr') ORDER BY score DESC");

		echo '<h4 class="ti">~ Leaderboard ~</h4>';
		echo '<ol>';
		while($liste = $req->fetch()){
			$a++;
		?>
			<li class="leader-<?php if($a==1)echo "first"; else if($a==2)echo "second"; else if($a==3)echo "third"; else echo "autre";?>">
				<span class="num"><?=$a?></span>
				<span class="img"><img src="images/avatar/png/<?=$liste['avatar']?>.png"/></span>
				<span class="nom"><span><?=limiteTexte($liste['nom'],10)?></span></span>
				<span class="points"><?=$liste['score']?> pt(s)</span>
			</li>
		<?php
		}
		echo '</ol>';
		echo '<h4 class="ti">~ Les non-inscrits ~</h4>';
		$req->closeCursor();


		$req = $db -> query("SELECT nom, score, (SELECT avatar FROM quiz_users WHERE quiz_users.username=nom) AS avatar FROM quiz_classement WHERE quiz_classement.nom IN (SELECT quiz_users.username FROM quiz_users WHERE email LIKE '%@tchatat.fr') ORDER BY score DESC");
		echo '<ol>';
		$a=0;
		while($liste = $req->fetch()){
			$a++;
		?>
			<li class="autres-<?php if($a==1)echo "first"; else if($a==2)echo "second"; else if($a==3)echo "third"; else echo "autre";?>">
				<span class="num"><?=$a?></span>
				<span class="img"><img src="images/avatar/png/<?=$liste['avatar']?>.png"/></span>
				<span class="nom"><span><?=limiteTexte($liste['nom'],10)?></span></span>
				<span class="points"><?=$liste['score']?> pt(s)</span>
			</li>
		<?php
		}
		echo '</ol>';
	}

function limiteTexte($text, $size) {
	    if (strlen($text) > $size)
	        return substr($text, 0, $size).' ...';
	    return $text;
	}
?>