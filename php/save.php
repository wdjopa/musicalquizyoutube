<?php

	include ("fonctions.php");

    if(isset($_GET['page']) && htmlentities($_GET['page'])){
        $username = htmlentities($_POST['username']);
        $username = strtolower($username);
     
        $avatar = htmlentities($_POST['avatar']);
       
        $req = $db -> query("SELECT * FROM quiz_users");

        $trouve = 0;
        while($donnees = $req -> fetch()){
            if(strtolower($donnees['username']) == $username){
                $trouve = 1;
            }
        }
        
        if(!$trouve){
            $req = $db -> prepare("INSERT INTO quiz_users(username,avatar,type) VALUES(:username,:avatar,'facebook')");
            $req -> execute(array("username"=>$username, "avatar"=>$avatar)); 
        }
        else{
            echo "Nom d'utilisateur déjà existant.";    
        }

    }
    else{
        
        $username = htmlentities($_POST['username']);
        
        $email = htmlentities($_POST['email']);

        $password = sha1(htmlentities($_POST['password']));

        $avatar = htmlentities($_POST['avatar']);

        $username = strtolower($username);
        $email = strtolower($email);
       
        $req = $db -> query("SELECT * FROM quiz_users");

        $trouve = 0;
        while($donnees = $req -> fetch()){
            if(strtolower($donnees['username']) == $username || strtolower($donnees['email']) == $email){
                $trouve = 1;
            }
        }
        
        if(!$trouve){
            $req = $db -> prepare("INSERT INTO quiz_users(username,email,password,avatar) VALUES(:username,:email,:password,:avatar)");
            $req -> execute(array("username"=>$username, "email"=>$email, "password"=>$password, "avatar"=>$avatar)); 
        }
        else{
            echo "Nom d'utilisateur déjà existant";    
        }

    }


?>