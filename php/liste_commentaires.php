<?php
	

	include ("fonctions.php");

	if(isset($_GET['type'])){

		$req = $db -> query("SELECT * FROM quiz_commentaires ORDER BY date DESC LIMIT 1");
		echo $req->fetch()['date'];
	}
	else{
		$a = 0;
		$req = $db -> query("SELECT * FROM quiz_commentaires ORDER BY date DESC LIMIT 30");

		while($liste = $req->fetch()){

			$rep = $db -> prepare("SELECT * FROM quiz_users WHERE username = :username");
			$rep -> execute(array('username' => $liste["username"]));

			$avatar = $rep->fetch()["avatar"];
			$nom = ucwords($liste["username"]);

			$i=0;
			$heure = explode(" ", $liste["date"])[1];

			$dates = explode("-", explode(" ", $liste["date"])[0]);

			$date="le ".$dates[2]."/".$dates[1]."/".$dates[0]." à ".$heure;
			echo '<div class="commentaire">';
			echo '<div class="profile">';
			echo '<figure>';
			echo '<img src="images/avatar/png/'.$avatar.'.png" alt="profile"/>';
			echo '</figure>';
			echo '</div>';
			echo '<div class="comment">';
			echo '<div class="info">';
			echo '<span>Posté par <span class="nom">'.$nom.'</span>, '.$date.'</span>';
			echo '</div>';
			echo '<div class="text">';
			echo $liste['message'];
			echo '</div>';
			echo '</div>';
			echo '</div>';
		}

	}

function limiteTexte($text, $size) {
	    if (strlen($text) > $size)
	        return substr($text, 0, $size).' ...';
	    return $text;
	}
?>