-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  jeu. 28 mars 2019 à 12:01
-- Version du serveur :  10.1.37-MariaDB
-- Version de PHP :  7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tchatat1_games`
--

-- --------------------------------------------------------

--
-- Structure de la table `quiz_classement`
--

CREATE TABLE `quiz_classement` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `score` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `quiz_commentaires`
--

CREATE TABLE `quiz_commentaires` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `quiz_users`
--

CREATE TABLE `quiz_users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `quiz_users`
--

INSERT INTO `quiz_users` (`id`, `username`, `avatar`, `type`, `email`, `password`) VALUES
(1, 'wdjopa', 'girl-1', '', 'sfd@sdf.fsd', '58718a46c1abe606ceb5765af2a6c5d3a473d2be'),
(2, 'anonyme1', 'girl-1', '', 'anonyme1@tchatat.fr', '00742588973edcd69b5a1745aa1735b9bf620c27'),
(3, 'anonyme2', 'girl-1', '', 'anonyme2@tchatat.fr', '56d9522d9428ee945c1af423771e97abaadecf0a');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `quiz_classement`
--
ALTER TABLE `quiz_classement`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `quiz_commentaires`
--
ALTER TABLE `quiz_commentaires`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `quiz_users`
--
ALTER TABLE `quiz_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `quiz_classement`
--
ALTER TABLE `quiz_classement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `quiz_commentaires`
--
ALTER TABLE `quiz_commentaires`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `quiz_users`
--
ALTER TABLE `quiz_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
