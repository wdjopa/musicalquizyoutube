<?php
	include "php/fonctions.php";
?>
<!DOCTPE html>
<html>
	<head>
		<meta charset="utf-8"/>
    	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
		<title>Musical Quizz, Passez de bons moments en musique</title>
		<link rel="icon" href="images/index.jpg"/>
		<link rel="stylesheet" href="fonts/polices.css"/>
		<link rel="stylesheet" href="css/loading-bar.css"/>
		<link rel="stylesheet" href="css/style.css"/>
		<link rel="stylesheet" href="fonts/font/flaticon.css"/>
		<meta property="og:title" content="Musical Quiz"/>
		<meta property="og:image" content="images/index.jpg"/>
		<meta property="og:url" content="musicalquiz.ovh"/>
		<meta property="og:site_name" content="musicalquiz.ovh"/>
		<meta property="og:description" content="Passez de bons moments en musique"/>
		<link rel="stylesheet" media="screen and (min-width: 800px)" href="css/style.large.css" type="text/css" />
		<link rel="stylesheet" media="screen and (max-width: 800px)" href="css/style.min.css" type="text/css" />
		<meta name="keywords" content="jeu de musique, blind test, jeu de quiz musical, quiz, musique, cameroun, quiz musical camerounais, quiz camerounais, jeunes camerounais, quiz musical, facebook quiz inside, inside, inside musique, tchatat quiz musical,tchatat quiz, games tchatat, tchatat djopa"/>
		<meta name="description" content="Jeu de quiz musical, Made By Tchat'At ."/>
		<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129183755-4"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-129183755-4');
	</script>

	</head>
	<body>

		<script>
			window.fbAsyncInit = function() {
				FB.init({
			      appId      : '159217381237967',
			      cookie     : true,
			      xfbml      : true,
			      version    : 'v2.10'
			    });		
			};
			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = 'https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.10&appId=159217381237967';
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>

		<div id="player" style="position:absolute;"></div>
		<div id="background-player" style="position:absolute;"></div>
		
		
		<div class="loader-body">
			<img src="images/svg/loader-tchatat.svg" alt="tchat'at" />
			<!--
			<video src="videos/background (2).mov" autoplay loop></video>
			-->
		</div>
		
		<section>
			

			<div class="audios hide-on-large">
				<h3 style="color: coral;">Nous utiliserons vos sorties audios.</h3>
				<br>
				<h4 style="color: dodgerblue;">Veuillez cliquer sur les boutons de lecture s'il vous plaît. Ensuite cliquez sur le bouton terminer</h4>
				<br>
				<br>
				<audio class="background-audio" loop autoplay volume="0.1"></audio>
				<br>
				<audio id="audio"></audio>
				<audio id="tonalite"></audio>
				<div class="part2 normal-connexion">
					<button class="btn coral" onclick="fin_config();">Démarrer</button>
				</div>
			</div>
			<!-- Page d'accueil -- Page n°1 - S'estompe automatiquement -->

			<div class="page page1 auto">

				<div class="part1 titre">Musical Quizz</div>
				<div class="second"><span class="text">Made for</span></div>
				<div class="part2 societe" style="text-align: center;"><a href="https://djopa.fr" target="_blank" style="color: goldenrod; text-decoration: none;">#Cameroon</a>
				<br>
				<div class="logos-link">
					<a href="http://facebook.com/willy.djopa" class="logo-link"><i class="flaticon-facebook"></i></a>
					<a href="http://linkedin.com/in/wilfried-djopa" class="logo-link"><i class="flaticon-linkedin"></i></a>
					<a href="http://twitter.com/willy_djopa" class="logo-link"><i class="flaticon-twitter"></i></a>
				</div>
			</div>
				<!-- <div class="second"><span class="text">sponsorisé par</span></div>
				<div class="part2 societe" style="text-align: center;"><a href="https://lamater.net" target="_blank" style="color: goldenrod; text-decoration: none;">La Mater Service</a><br><small style="color: darkgoldenrod; font-size: 3vh;">Votre marché chez vous !</small></div> -->
			</div>
			
			<!-- Page de connexion -- Page n°2 -->
			<div class="page page2">
				<div class="part1 face-connexion">
					<!--
					<div id="status" class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="true" onlogin="checkLoginState();" scope="public_profile,email"></div>
					-->
					<button class="btn" onclick="page(3, 2, 1)">S'inscrire</button>
				</div>
				<div class="second"><span class="text">ou</span></div>
				<div class="part2 normal-connexion">
					<button class="btn btn2 coral" onclick="page(3, 2, 2)">Se connecter</button>
				</div>
				<div class="second"><span class="text">ou</span></div>
				<div class="part2 normal-test">
					<button class="btn btn3 orange" onclick="partie_directe()">Partie rapide</button>
				</div>
				<div class="second"><span class="text">Je suis inspiré</span></div>
				<div class="part2 normal-test">
					<button class="btn btn4" style="background: goldenrod;border-color: goldenrod;" onclick="window.location.href='./admin/?page=admin'">Ajouter mes questions</button>
				</div>
			</div>
			
			<!-- Page de connexion -- Page n°3 -->
			<div class="page page3">

				<div class="forms inscription-form">
					<h2 class="titre">Inscription</h2>
					<!--
					<h4 class="error">Les mots de passe ne correspondent pas !</h4>
					-->
					<div class="form">
						<p class="block">
							<input type="text" class="champ" id="username" placeholder="Nom d'utilisateur" />
						</p>
						<p class="block">
							<input type="email" class="champ" id="email" placeholder="Adresse électronique" />
						</p>
						<p class="block">
							<input type="password" class="champ" id="password" placeholder="Mot de passe" />
						</p>
						<p class="block">
							<input type="password" class="champ" id="confirm_password" placeholder="Confirmer le mot de passe" />
						</p>
						<p class="block">
							<input onclick="inscription('valider')" type="button" class="champ btn" value="Inscription" />
						</p>
						<div class="second"><span>Déjà inscris ?</span></div>
						<p class="block">
							<input onclick="connexion('ouvrir')" type="button" class="champ btn autre" value="Connexion" />
						</p>
						<div class="second"><span>Retourner au Menu</span></div>
						<p class="block">
							<input onclick="page(2)" type="button" class="champ btn violet" value="Retour" />
						</p>
					</div>
				</div>

				<div class="forms connexion-form">
					<h2 class="titre">Connexion</h2>
					<!-- 
					<h4 class="error">Mot de passe ou nom d'utilisateur incorrect !</h4>
					-->
					<div class="form">
						<p class="block">
							<input type="text" class="champ username" placeholder="Nom d'utilisateur" autofocus value=""/>
						</p>
						<p class="block">
							<input type="password" class="champ password" placeholder="Mot de passe" value="" />
						</p>
						<p class="block">
							<input onclick="connexion('valider')" type="button" class="champ btn" value="Connexion" />
						</p>
						<div class="second"><span>Pas encore de compte ?</span></div>
						<p class="block">
							<input onclick="inscription('ouvrir')" type="button" class="champ btn autre" value="Inscription" />
						</p>
					</div>
				</div>
				<div class="forms forgotten-password">
					<h2 class="titre">Mot de passe oublié</h2>
					<!-- 
					<h4 class="error">Mot de passe ou nom d'utilisateur incorrect !</h4>
					-->
					<div class="form">
						<p class="block">
							<input type="email" class="champ email" placeholder="Adresse électronique" autofocus value=""/>
						</p>
						<p class="block">
							<input onclick="forgottenpassword('valider')" type="button" class="champ btn" value="Envoyer" />
						</p>
						<div class="second"><span>Abandonner la procédure ?</span></div>
						<p class="block">
							<input onclick="connexion('ouvrir')" type="button" class="champ btn" value="Connexion" />
						</p>
					</div>
				</div>
			</div>
			
			<!-- Page de présentation, choix de son avatar -- Page n°4 -->			
			<div class="page page4">
				<div class="part1 ">Salut <span class="username"></span>,</div>
				<div class="choix avatar">
					<h4 class="titre">Choisissez votre avatar</h4>
					<div class="container-avatar">
						<h4>Chargement des avatars</h4>
						<img src="images/svg/loader-2.svg" class="load"/>
					</div>
				</div>
				<div class="part2 ">
					<input type="button" class="btn" onclick="suite()" value="Continuer"/>
				</div>
			</div>
			
			<!-- Page du quizz -- Page n°5 -->
			<div class="page page5">
				<div class="fin">
					Game Over
				</div>
				<div class="container-quiz">
					<div class="bloc-gauche">
						<div class="bloc-gauche-haut">
							<div class="col-1 col entete">
								<div class="titre">Musical Quizz</div>
								<div class="user">
									<span class="username"><span class="username"></span></span>
									<figure class="avatar">
										<img class="avatar-img" src="images/avatar/png/boy.png" alt="boy.png"/>
									</figure>
								</div>
							</div>
							<div class="box1 hide-on-large top">
								<div class="score">
									<span class="text">Score</span>
									<span class="valeur">0</span>
								</div>
								
								<div class="timer">
									<span class="text">Temps Restant</span>
									<span class="valeur">3'00</span>
								</div>		
							</div>
						</div>
						<div class="bloc-gauche-bas">
							<div class="col-3 questions">
								<span class="texte">
									Quelle est l'origine du rappeur en featuring avec Booba ?
								</span>

								<span class="loader-musique">
									<img src="images/svg/loader-2.svg" alt="chargement"/>
								</span>
							</div>
							<div class="col-4 propositions">
								<div class="ligne1 ligne">
									<div class="col1 col proposition prop1">
										<span class="btn-proposition proposition prop1">Proposition 1</span>
									</div>
									<div class="col2 col proposition prop2">
										<span class="btn-proposition proposition prop2">Proposition 2</span>
									</div>
								</div>
								<div class="ligne2 ligne">
									<div class="col1 col proposition prop3">
										<span class="btn-proposition proposition prop3">Proposition 3</span>
									</div>
									<div class="col2 col proposition prop4">
										<span class="btn-proposition proposition prop4">Proposition 4</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="hide-on-small bloc-droite ">
						<div class="box1 top">
							<div class="score">
								<span class="text">Score</span>
								<span class="valeur">0</span>
							</div>
							
							<div class="col-2 col timer">
								<span class="text">Temps Restant</span>
								<span class="valeur">0</span>
								<!--
								<div
								  data-type="fill"
								  data-path="M10 10L90 10L90 90L10 90Z"
								  class="ldBar label-center"
								  style="width:100%;height:20%;margin:auto"
								  data-fill="data:ldbar/res,bubble(dodgerblue,#fff,50,20)"
								  data-value="100"
								></div>

								<div onclick="message('Il s\'agit du nombre de questions trouvées par rapport aux questions ratées.','',5000)"
							  class="ldBar label-center"
							  style="width:100%;height:50%;margin:auto"
							  data-value="100"
							  data-preset="bubble"
							  data-fill="data:ldbar/res,bubble(orangered,#fff,50,20)"
							></div>
								-->
							</div>
							
						</div>
						<div class="box2 center">
							<div class="fb-page" data-href="https://www.facebook.com/Inside-124075274956987/" data-width="280" data-hide-cover="false" data-show-facepile="false"></div>
							<div class="fb-follow" data-href="https://www.facebook.com/inside-124075274956987/" data-width="270" data-layout="standard" data-size="large" data-show-faces="true"></div>
							
						</div>
						<div class="box3 bottom">
							<!--
							<div class="contacts">
								<span class="text">Suivez-nous</span>
								<div class="link">

								</div>
							</div>-->
							<div class="copyright">
								Copyright &#169; <a href="https://www.djopa.fr/" target="_blank" style="color: white;">Tchat'At</a> 2017 - 2019.
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="page page6">
				<div class="part1 texte">
					<div class="par1">
						Hey <em><span class="username"></span></em>, j'espere que tu t'es bien amuse(e) ^_^...
					</div>
					<div class="par2">
						Tu as fais un score de <em class="score"></em> ...<br>
						Tu es <em class="rang"></em> au classement. 
					</div>
				</div>
				<div class="part2 ">
					<input type="button" class="btn" onclick="page(7)" value="Continuer"/>
				</div>
			</div>
			<div class="page page7">
				<div class="part1">
					<div class="commentaires">
						<div class="new-comment">
							<div class="entete">
								Commentaires
							</div>
							<div class="comment">
								<!--<div class="texte">
									Un petit commentaire <span class="username"></span> ?
								</div>-->
								<div class="profile">
									<figure>
										<img class="avatar-img"/>
									</figure>
								</div>
								<div class="text">
									<textarea placeholder="Ecrivez votre commentaire ici ...Nous seront ravis de lire vos commentaires" autofocus></textarea>
								</div>
							</div>
							<div class="send">
								<button class="btn" onclick="save_comment()">Envoyer</button>
							</div>
						</div>
						<div class="comments">
							
						</div>
					</div>
				</div>
				<div class="part2">
					<input type="button" class="btn" onclick="page(8)" value="Continuer"/>
				</div>
			</div>		
			<div class="page page8">
				<div class="part1">
					<button class="btn" onclick="page(9)">Voir le classement</button>
				</div>
				<div class="second"><span class="text">1</span></div>
				<div class="part2">
					<button class="btn coral" onclick="rejouer()">Refaire une partie</button>
				</div>
				<div class="second"><span class="text">2</span></div>
				<div class="part2">
					<button class="btn coral" onclick="page(7)">Commenter</button>
				</div>
				<div class="second"><span class="text">3</span></div>
				<div class="part2">
					<button class="btn violet" onclick="page(2)">Quitter</button>
				</div>
			</div>		
			<div class="page page9">
				<div class="entete">
					<h3> Musical . Quiz</h3>
				</div>
				<div class="corps">
					<div class="liste">
						
					</div>
				</div>
				<div class="part2">
					<button class="btn coral" onclick="page(8,9)">Retour</button>						
				</div>
			</div>		
		</section>
		<div class="messages">

		</div>
		
		<script>
		</script>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
			var questions = [];
	
			function setRandomPicture(){
				let pics = [];
				<?php
					$dir = "images/backgrounds/";
					$dh  = opendir($dir);
					while (false !== ($fichier = readdir($dh))) {
					if($fichier !== "." && $fichier!=".."){
						?>
						pics.push("images/backgrounds/<?=$fichier;?>");
						<?php
					}
				}
				?>
				let pic = pics[random(0,pics.length)];
				
				document.querySelector("body").style.backgroundImage = "url('"+pic+"')";
			}

			// if(Website2APK){

			// 	(adsbygoogle = window.adsbygoogle || []).push({
			// 		google_ad_client: "ca-pub-5080349210644365",
			// 		enable_page_level_ads: true
			// 	});
				
			// 	Website2APK.showInterstitialAd();
			// }

		</script>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/loading-bar.js"></script>
		<script type="text/javascript" src="js/tchatat.js"></script>
		<script type="text/javascript" src="js/strength.js"></script>
		<script type="text/javascript" src="js/yout.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
		<script type="text/javascript" src="js/js.js"></script>
		<script>
		</script>
	</body>
</html>