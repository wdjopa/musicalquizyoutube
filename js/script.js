var liste = questions;
var question_bloc = document.querySelector(".bloc-gauche-bas .questions .texte");
var propositions_bloc = document.querySelectorAll(".bloc-gauche-bas .propositions .col");
var score =  document.querySelector(".score .valeur");
var num,reponse,nombr=1,total = questions.length;
var cliquable = 1;
var intervalle_question = 1000; //Intervalle de temps entre la réponse et le passage a la question suivante
var user = {};
var debut = new Date();
var pause = 0, duree = 180, seconds=duree;
var faux = 0;
var fini = 1;


if(getCookie("user")!=""){
	user = JSON.parse(getCookie("user"));
	miseajour(user.name, user.avatar_id)
}

$(window).blur(function(){
	pause = 1;
	if(fini == 0){
		pauseVideo();
	}
	else{
		pauseBackground();	
	}
	
});
$(window).focus(function(){
	if(fini == 0){
		playVideo();	
	}
	else{
		playBackground();	
	}
	pause = 0;
});

function miseajour(username, avatar){
	user.name = username;
	user.avatar = avatar;


	for(var i=0;i<document.querySelectorAll('.username').length;i++){
		document.querySelectorAll('.username')[i].innerHTML = limiteTexte(user.name, 15);
	}
	for(var i=0;i<document.querySelectorAll('.avatar-img').length;i++){
		document.querySelectorAll('.avatar-img')[i].setAttribute("src", "images/avatar/png/"+user.avatar+".png");
		document.querySelectorAll('.avatar-img')[i].setAttribute("alt",user.avatar);
	}		
}

function rejouer(){
	//effacer la page de game over
	document.querySelector(".page5 .fin").style.display = "none";
	for(var i=0;i<document.querySelectorAll(".score .valeur").length;i++)
		document.querySelectorAll(".score .valeur")[i].innerHTML = 0;
	cliquable = 1;
	liste = questions;
	nombr=1;
	seconds=duree;
	pause = 0;
	faux = 0;
	fini = 0;
	page(5, 8);
	lancer();
}

function start(){
	//document.querySelector("section .corps .page").style.display="none";
	//document.querySelector(".page.connexion").style.display="none";
	//document.querySelector(".page.game").style.display="flex";
	compteur(duree,".timer .valeur");
	question();
	fini = 0;
	pauseBackground();
	
	// document.querySelector("#tonalite").volume = 1;
	// document.querySelector("#audio").volume = 1;
	//setInterval(scores,1000);
}


function save(){
	$.post("php/enregistrer.php",{nom:user.name, id_face:user.id,score:document.querySelector(".score .valeur").innerHTML}, function (data){
	});
	//document.querySelector("section .corps .page").style.display="flex";
	//document.querySelector("section .corps .page .beginner").style.display="none";
	//document.querySelector("section .corps .page .after").style.display="flex";
	//window.location.reload();
}

function scores(){
	$.post("php/liste_scores.php", function (data){
		if(data != "")
			document.querySelector(".page9 .corps .liste").innerHTML = data;
		else
			document.querySelector(".page9 .corps .liste").innerHTML = "Aucun score n'a encore été enregistré.";
	});
}
function commentaires(){
	$.post("php/liste_commentaires.php", function (data){
		if(data != "")
			document.querySelector(".page7 .commentaires .comments").innerHTML = data;
		else
			document.querySelector(".page7 .commentaires .comments").innerHTML = "Aucun commentaire n'a encore été posté.";
	});
}
function highscore(){
	$.post("php/liste_scores.php?type=highscore", function (data){
		if(data != "")
			document.querySelector(".navigation .high-score .valeur").innerHTML = data;
		else
			document.querySelector(".navigation .high-score .valeur").innerHTML = 0;
	});
}


function question(){

	if(nombr <= total ){
		if(pause == 0){
			pause = 1;
			cliquable = 0;

			// document.querySelector("#audio").pause();

			//document.querySelector(".corps .question .entete").innerHTML = "Question "+nombr+" / "+total;
			nombr++;

			// if(nombr >= 3)
				// document.querySelector("#audio").removeAttribute("controls"); // Pour la version mobile
			num = random(0,liste.length); //On prend un élément au hasard dans le tableau
		
			liste[num].question = d(liste[num].question);		
			liste[num].reponse = d(liste[num].reponse);		
			liste[num].propositions = d(liste[num].propositions);		
			liste[num].musique = d(liste[num].musique);		

			var props = melange(liste[num].propositions.split(",")); // On prend les propositions et on les mélangea avant de les mettre dans props
			
			//On affiche le tout
			reponse = liste[num].reponse;

			question_bloc.innerHTML = liste[num].question;
			for(var i=0;i<propositions_bloc.length;i++){
				propositions_bloc[i].innerHTML = props[i];
			}
			for(var i=0;i<propositions_bloc.length;i++){
				document.querySelector(".proposition.prop"+(i+1)).style.animation = "";	
				propositions_bloc[i].setAttribute("onclick","clique(\""+props[i]+"\", "+i+")"); 
			}
			//question_bloc.innerHTML = liste[num].question;

			//On lance la musique
			playAudio(liste[num].musique,liste[num].debut);			
			// document.querySelector("#audio").src=liste[num].musique;
			// document.querySelector("#audio").currentTime=parseInt(liste[num].debut);


			// document.querySelector("#audio").onloadstart = function(){
			// 	document.querySelector(".loader-musique").style.display = "inline-block";
			// 	pause = 1;
			// 	cliquable = 0;
			// }
			// document.querySelector("#audio").onerror = function(){
			// 	pause = 0;
			// 	cliquable = 1;
			// 	next_question();
			// 	console.log("error occured");
			// }
			// document.querySelector("#audio").oncanplay = function(){
			// 	document.querySelector(".loader-musique").style.display = "none";
			// 	document.querySelector("#audio").play();
			// 	pause = 0;
			// 	cliquable = 1;
				
			// }
			//On supprime ce numéro
			supprbypos(num, liste);
		}
	}
	else{
		fin();
	}

}
function clique(texte, num){
	if(cliquable == 1){
		cliquable = 0;
		pause = 1;
		if(texte == reponse){
			document.querySelector("#tonalite").src="audios/win2.wav";
			document.querySelector("#tonalite").currentTime=0;
			document.querySelector("#tonalite").volume=0.25;
			document.querySelector("#tonalite").oncanplay = function(){
				document.querySelector("#tonalite").play();
			}
			correct(num);
		}
		else{
			document.querySelector("#tonalite").src="audios/lost.mp3";
			document.querySelector("#tonalite").currentTime=0;
			document.querySelector("#tonalite").volume=0.25;
			document.querySelector("#tonalite").play();
			document.querySelector("#tonalite").oncanplay = function(){
				document.querySelector("#tonalite").play();
			}
			incorrect(num);
		}
		//document.querySelector(".corps .infos .reponse .valeur").innerHTML = reponse;
		setTimeout(next_question, intervalle_question);
/*
		var val = (nombr-faux) * 100 / nombr;
  		bar.set(val);*/
	}
}

function next_question(){
	pause = 0;
	question();
}

function correct(num){
	for(var i=0;i<document.querySelectorAll(".score .valeur").length;i++)
		document.querySelectorAll(".score .valeur")[i].innerHTML = parseInt(document.querySelectorAll(".score .valeur")[i].innerHTML) + 1;
	document.querySelector(".proposition.prop"+(num+1)).style.animation = "trouve 0.3s ease 5";
	intervalle_question = 1500;
}

function incorrect(num){
	document.querySelector(".proposition.prop"+(num+1)).style.animation = "rate 0.3s ease 5";
	faux++;
	setTimeout(function(){
		for(var i=0;i<propositions_bloc.length;i++){
			if(propositions_bloc[i].innerHTML == reponse){
				document.querySelector(".proposition.prop"+(i+1)).style.animation = "trouve 0.3s ease 5";
			}
		}
	}, 1500);
	intervalle_question = 3000;
}

function pause_music(){
	if(pause == 0){
		pause = 1;
		document.querySelector("#audio").pause();
	}
	else{
		pause = 0;
		document.querySelector("#audio").play();
	}
}

function rang(){
	$.post("php/rang.php", {nom:user.name, score:document.querySelector(".score .valeur").innerHTML}, function (data){
		if(parseInt(data) == 1)
			document.querySelector(".rang").innerHTML = "1<sup>er(e)</sup";
		else
			document.querySelector(".rang").innerHTML = parseInt(data)+"<sup>ème</sup";
	});
}

function fin(){
	pause = 1;
	fini = 1;
	cliquable = 0;
	pauseVideo();
	playBackground();
	//change('scores-logo');

	rang();
	save();
	
	var sc = document.querySelector(".score .valeur").innerHTML;
	var ch = (sc<=1)?"pt":"pts";

	document.querySelector("em.score").innerHTML = sc+ch;

	document.querySelector(".background-audio").volume = 0.5;
	document.querySelector(".page5 .fin").style.display = "flex";
	document.querySelector(".page5 .fin").style.animation = "entree 1s ease";

	document.querySelector(".loader-body").innerHTML = '<img src="images/svg/loader-tchatat.svg" alt="tchat\'at" />';
	document.querySelector(".loader-body").style.background = "0% 0% / cover #1919165c";
	document.querySelector(".loader-body").style.backgroundSize = "cover";
	
	setTimeout(page,3500,6);
}


function compteur(temps, elt){ 
	if(!temps)
		temps = 0;
	if(pause == 0){
		seconds = temps;
		temps--;
	}
	second(seconds, elt);
//	(pas > reponses.length)?document.querySelector(elt).innerHTML = 0: setTimeout(compteur, 905, temps);		
	if (seconds <= 0){
		fin();
		//pas = reponses.length +5 ;
	}
	else{
		setTimeout(compteur, 980, temps, elt);
	}
}

// if(Website2APK){
// 	Website2APK.showInterstitialAd();
// }

function second(a,elt){
	
	b = parseInt(a/60);
	if(b == 0){
		for(var i=0;i<document.querySelectorAll(elt).length;i++)	
			document.querySelectorAll(elt)[i].innerHTML = a;
	}
	else{
		a = a - (b * 60); 
		for(var i=0;i<document.querySelectorAll(elt).length;i++)	
			document.querySelectorAll(elt)[i].innerHTML = b + "'"+a;
	}
	
}
