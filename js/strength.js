
var characters = 0;
var capitalletters = 0;
var loweletters = 0;
var number = 0;
var special = 0;

var upperCase= new RegExp('[A-Z]');
var lowerCase= new RegExp('[a-z]');
var numbers = new RegExp('[0-9]');
var specialchars = new RegExp('([!,%,&,@,#,$,^,*,?,_,~])');

function check_strength(elt){
    var thisval = elt.value;
    if (thisval.length > 8) { characters = 1; } else { characters = -1; };
    if (thisval.match(upperCase)) { capitalletters = 1} else { capitalletters = 0; };
    if (thisval.match(lowerCase)) { loweletters = 1}  else { loweletters = 0; };
    if (thisval.match(numbers)) { number = 1}  else { number = 0; };

    var total = characters + capitalletters + loweletters + number + special;

    if (!thisval.length) {total = -1;}

    return total;
}
