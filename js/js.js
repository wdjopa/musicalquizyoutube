/*
*	Penser a mettre un bouton qui propose a l'utilisateur de quitter du mode PLein ecran a tout moment	
*
*/

		var window_focus = true;
		var passagePage2 = 8000;
		var username = document.querySelector("#username").innerHTML;
		var email = document.querySelector("#email").innerHTML;
		var password = document.querySelector("#password").innerHTML;
		var password2 = document.querySelector("#confirm_password").innerHTML;
		var emos, avatar_id="";
		var travail = 0; //bloque les clics de l'utilisateur sur un bouton a plusieurs reprises
		var aud = 0;
		var onplaying = true;
		var onpause = false;
/*
		setTimeout(function (){
			message("Rappel: Si vous voulez quitter du mode plein écran, appuyez sur <b>ECHAP</b>", "info hide-on-small", 4000);
		}, (3*60*1000));*/

		setRandomPicture();

		//Fixer les propriétés de la musique de fond
		setTimeout(function(){
			playAudioBackground(playRandomBackground(), 15);
		},100)

		setTimeout(
			function(){
				if(!player){
					playAudioBackground(playRandomBackground(), 15);
				}
			},
		1000)


		document.querySelector(".background-audio").volume = 0.3;
		document.querySelector(".background-audio").currentTime = 15;
		let audioBack = document.querySelector(".background-audio");
			
		var isPlaying = audioBack.currentTime > 0 && !audioBack.paused && !audioBack.ended 
			&& audioBack.readyState > 2;

		if (!isPlaying) {
			audioBack.play();
		}

		window.onblur = function() { window_focus = false; }
		window.onfocus = function() { window_focus = true; }

		document.querySelector(".background-audio").addEventListener("play", function(){
			document.querySelector(".background-audio").setAttribute("class", "background-audio hide-on-small hide-on-large");
			aud++;
			if(aud == 3){
				fin_config();
			}
		});
		document.querySelector("#audio").addEventListener("play", function(){
			document.querySelector("#audio").setAttribute("class", "hide-on-small hide-on-large");
			aud++;
			if(aud == 3){
				fin_config();
			}
		});
		document.querySelector("#tonalite").addEventListener("play", function(){
			document.querySelector("#tonalite").setAttribute("class", "hide-on-small hide-on-large");
			aud++;
			if(aud == 3){
				fin_config();
			}
		});

		//Recuperation des questions
		$.get("php/questions.php",{asd:"sfd"},function(data){
			let a = JSON.parse(data);
			a.forEach((q)=>{
				questions.push(q);
				q.question = c(q.question);
				q.reponse = c(q.reponse);
				q.propositions = c(q.propositions);
				q.musique = c(q.musique);
			})
		})

		initializeAll();

		function initializeAll(){
			console.log(user)
			if(JSON.stringify(user) != "{}"){
				$(".page2 .part2 .btn2").text("Jouer en tant que "+user.name);
				document.querySelector(".page2 .part2 .btn2").setAttribute("onclick","lancer()");
				$(".page2 .part2 .btn3").text("Déconnexion");
				document.querySelector(".page2 .part2 .btn3").setAttribute("onclick","deconnexion()");
				document.querySelector(".page2 .part2 .btn3").setAttribute("class","btn btn3 violet");
			}
		}

		function deconnexion(){
			deleteCookie("user");
			window.location.reload();
		}

		function playRandomBackground(){
			let songs = [];
			questions.forEach(function (question){
				songs.push(d(question.musique));
			})
			let song = songs[random(0,songs.length)];
			return song;
		}

		function miseajour(username, avatar){
			user.name = username;
			user.avatar = avatar;


			for(var i=0;i<document.querySelectorAll('.username').length;i++){
				document.querySelectorAll('.username')[i].innerHTML = limiteTexte(user.name, 15);
			}
			for(var i=0;i<document.querySelectorAll('.avatar-img').length;i++){
				document.querySelectorAll('.avatar-img')[i].setAttribute("src", "images/avatar/png/"+user.avatar+".png");
				document.querySelectorAll('.avatar-img')[i].setAttribute("alt",user.avatar);
			}		
		}

		function fin_config(){
			
			document.querySelector(".audios").style.display = "none";
			document.querySelector(".background-audio").play();
			document.querySelector("#tonalite").play();
			message("Merci. Nous vous souhaitons de passer un bon moment !", "info", 3000);
		}

		//Delai d'attente pour passer a la page 2
		setTimeout(function (){
			document.querySelector(".page1").style.display = "none";
			page(2);
		}, passagePage2);
/*
		setTimeout(function (){
			message("Cliquez sur l'écran pour activer le mode plein écran.", "info", 5000);
		}, 2000);*/

		function save_comment(){
			var texte = document.querySelector("textarea").value;
			if(texte.length>0){
				$.post("php/save_comment.php",{message:texte, username:user.name}, function (data){
					if(!data){
						message("Votre message a été posté avec succès");
					}
					else{
						message("Erreur lors du post du message");
					}
					document.querySelector("textarea").value = "";
					document.querySelector("textarea").focus();
				});
			}
			else{
				message("Veuillez écrire un message.");
			}
		}	

		function forgottenpassword(etat){
			if(!travail){
				travail = 1;
				if(etat == "ouvrir"){
					
					for(var i=0;i<document.querySelectorAll(".forms").length;i++){
						document.querySelectorAll(".forms")[i].style.opacity = "0";	
						document.querySelectorAll(".forms")[i].style.transform = "scale(0)";	
						document.querySelectorAll(".forms")[i].style.display = "none";	
					}
					document.querySelector(".forgotten-password").style.transform = "scale(1)";	
					document.querySelector(".forgotten-password").style.opacity = "1";	
					document.querySelector(".forgotten-password").style.display = "flex";	
					document.querySelector(".forgotten-password").style.animation = "entree 1s ease";

					travail = 0;
				}
				else{
					var email = document.querySelector(".forgotten-password .email");
					if(email.value.length<6){
						travail = 0;
						message("Veuillez entrer une email valide", "erreur", 5000);
					}
					else{
						$.post("php/verify_email.php", {email:email.value}, function(data){
							if(data){
								$.post("php/email.php", {email:email.value}, function(data2){
									if(data2){
										travail = 0;						
										message("Erreur:"+data2);
									}
									else{
										message("Envoi en cours ... Veuillez patienter", "info", 4000);
										$.post("php/change.php", {email:email.value}, function(data){
											message("Veuillez consulter vos mails. (Le compte "+email.value+")", "info", 4000);
										});
										page(3);
									}
								});
							}
							else{
								travail = 0;
								message("Cette adresse mail n'est pas présente dans notre base de données.", "erreur", 4000);
							}
						});
					}
				}
			}
		}
		function connexion(etat){
			if(!travail){

				travail = 1;
				if(etat == "ouvrir"){
					for(var i=0;i<document.querySelectorAll(".forms").length;i++){
						document.querySelectorAll(".forms")[i].style.opacity = "0";	
						document.querySelectorAll(".forms")[i].style.transform = "scale(0)";	
						document.querySelectorAll(".forms")[i].style.display = "none";	
					}
					document.querySelector(".connexion-form").style.transform = "scale(1)";	
					document.querySelector(".connexion-form").style.opacity = "1";	
					document.querySelector(".connexion-form").style.display = "flex";	
					document.querySelector(".connexion-form").style.animation = "entree 1s ease";

					travail = 0;
				}
				else{
					var username = document.querySelector(".connexion-form .form .username").value.trim();
					var password = document.querySelector(".connexion-form .form .password").value;
					if(username.length > 0 && password.length > 0){
						message("Connexion en cours ... Veuillez patienter");
						$.post("php/connexion-form.php", {username:username, password:password}, function (data){
							if(data == "erreur_user"){
								message("Le nom d'utilisateur n'est pas valide.", "erreur", 6000);
								setTimeout(message, 3000, "Si vous ne vous êtes jamais inscrits, inscrivez-vous !", "info", 5000);
								travail = 0;
							}
							else if(data == "erreur_pass"){
								message("Le mot de passe n'est pas valide.", "erreur", 6000);
								setTimeout(message, 3000, "Si vous ne vous avez oublié le mot de passe <button onclick=forgottenpassword('ouvrir')>Cliquez ici</button>!", "info", 5000);
								travail = 0;
							}
							else{
								message("Connexion établie");
								message("Données de connexion récupérées ...");
								miseajour(data.split("**")[0], data.split("**")[1]);
								
								// document.querySelector(".page3").style.opacity = "0";
								// document.querySelector(".page3").style.transform = "scale(0)";
								// document.querySelector(".page3").style.animation = "dissipe 1s ease";
								console.log(JSON.stringify({"name":data.split("**")[0], "avatar_id":data.split("**")[1]}));
								setCookie("user",JSON.stringify({"name":data.split("**")[0], "avatar_id":data.split("**")[1]}),1000);
								initializeAll();
								// lancer();
								page(2);
							}
						});
					}
					else{
						travail = 0;		
						if(password.value.length<=0){
							message("Vous n'avez pas entré de mot de passe ... ", "erreur", 6000);
							password.focus();
						}
						else{	
							message("Vous n'avez pas entré un nom d'utilisateur ... ", "erreur", 6000);
							username.focus();
						}
					}
				}
			}
		}	

		function inscription(etat){
			if(!travail){
				travail = 1;	
				if(etat == "ouvrir"){
					
					for(var i=0;i<document.querySelectorAll(".forms").length;i++){
						document.querySelectorAll(".forms")[i].style.opacity = "0";	
						document.querySelectorAll(".forms")[i].style.transform = "scale(0)";	
						document.querySelectorAll(".forms")[i].style.display = "none";	
					}
					document.querySelector(".inscription-form").style.transform = "scale(1)";	
					document.querySelector(".inscription-form").style.opacity = "1";	
					document.querySelector(".inscription-form").style.display = "flex";	
					document.querySelector(".inscription-form").style.animation = "entree 1s ease";

					/*
					document.querySelector(".connexion-form").style.opacity = "0";	
					document.querySelector(".connexion-form").style.transform = "scale(0)";	
					document.querySelector(".connexion-form").style.display = "none";	
					*/
					travail = 0;	
				}
				else{

					username = document.querySelector("#username");
					email = document.querySelector("#email");
					password = document.querySelector("#password");
					password2 = document.querySelector("#confirm_password");
					
					if(!(password.value.length > 0 && password2.value.length > 0 && email.value.length > 0 && username.value.length > 0)){
						if(password.value.length<=0){
							message("Vous n'avez pas entré de mot de passe ... ", "erreur", 6000);
							password.focus();
						}
						else if(password2.value.length<=0){
							message("Vous n'avez pas entré la confirmation du mot de passe ... ", "erreur", 6000);
							password2.focus();
						}
						else if(email.value.length<=0){
							message("Vous n'avez pas entré d'adresse électronique ... ", "erreur", 6000);
							email.focus();
						}
						else if(username.value.length<=2){	
							if(username.value.length<=0){	
								message("Vous n'avez pas entré un nom d'utilisateur ... ", "erreur", 6000);
							}
							else{
								message("Le nom d'utilisateur doit avoir au moins 3 caractères ... ", "erreur", 6000);	
							}
							username.focus();
						}
						travail = 0;	
					}
					else if(password.value != password2.value){
						travail = 0;	
						message("Les mots de passe ne sont pas les mêmes ... ", "erreur", 6000);
						password2.focus();
					}
					else{
						if(!isEmail(email.value)){
							travail = 0;
							message("Cette adresse électronique n'est pas conforme ... ", "erreur", 6000);
							email.focus();		
						}
						else{
							if(check_strength(password) < 2){
								travail = 0;
								message("Le niveau de sécurité de votre mot de passe est faible ...", "erreur", 6000);
								setTimeout(message, 3000, "Il doit avoir au moins 2 des 4 caracteristiques suivante:<br>1. Au moins 8 caracteres;<br>2. Une majuscule;<br>3. Un chiffre;<br>4. Un caractere spécial.", "info", 12000);
								password.focus();				
								password2.value = "";				
							}
							else{
								travail = 0;
								document.querySelector(".page4 .username").innerHTML = username.value;
								page(4);
								emoticones();
							}
						}
					}
				}
			}
		}	

		function lancer(){
					
			total = questions.length
			//console.log(total);
			let audioCompteur = setInterval(function (){
				if(document.querySelector(".background-audio").volume > 0.05)
				document.querySelector(".background-audio").volume-=0.05;
			}, 200);
			setTimeout(function (){
				clearInterval(audioCompteur);
				document.querySelector(".loader-body").innerHTML = "";
				document.querySelector(".loader-body").style.background = "url('images/index.jpg')";
				document.querySelector(".loader-body").style.backgroundSize = "cover";
				page(5);
				document.querySelector("#audio").setAttribute("loop","true");

				/*document.querySelector(".box1.top").innerHTML += '<div onclick=\'message("Il s\'agit du nombre de questions trouvées par rapport aux questions ratées.","",5000)\' class="ldBar label-center"style="width:100%;height:50%;margin:auto" data-value="100"data-preset="bubble" data-fill="data:ldbar/res,bubble(orangered,#fff,50,20)"></div>';*/
			}, 2000);
			setTimeout(start, 3500);
		}
		function suite(num = 1){	
			if(!travail){
				travail = 1;
				if(num == 1){
					if(avatar_id == ""){
						message("Cliquez sur un avatar s'il-vous-plaît", "error");
					}
					else{
						document.querySelector(".page4 .username").innerHTML = username.value;
						message("Nous vérifions vos informations", "", 5000);
						$.post("php/save.php", {username:username.value.trim(), email:email.value.trim(), password:password.value, avatar:avatar_id}, function (data){
							console.log(data);
							if(data){
								travail = 0;
								message("Ce nom d'utilisateur et/ou cette adresse email existe(nt) déjà ...", "erreur", 6000);
								page(3, 4, 1);
							}
							else{
								message("Vos informations ont été enregistrées avec succès", "", 5000);
								message("Bienvenue "+username.value, "", 5000);
								setCookie("user",JSON.stringify({"name":username.value, "avatar_id":avatar_id}),1000);
								miseajour(username.value, avatar_id);
								initializeAll();
								page(2);
							}
						});
					}
				}
				else if(num == 2){//choix d'avatar apres Connexion par facebook, 
					
					if(avatar_id == ""){
						message("Cliquez sur un avatar s'il-vous-plaît", "error");
					}
					else{
						message("Nous vérifions vos informations", "", 5000);
						$.post("php/save.php?page=face", {username:user.name,avatar:avatar_id}, function (data){
							if(data){
								travail = 0;
								message("Ce nom d'utilisateur existe(nt) déjà ...", "erreur", 6000);
							}
							else{
								message("Vos informations ont été enregistrées avec succès", "", 5000);
								message("Bienvenue "+user.name, "", 5000);
								miseajour(user.name, avatar_id);
								lancer();
							}
						});

					}
				}
			}
		}
		
		function message(texte = "Erreur", type = "normal", time = 3000){
			var span = document.createElement("span");
			span.setAttribute("class", "message "+type);
			if(type == "info")
				span.innerHTML = "<span class='circle'>i</span> "+texte;
			else
				span.innerHTML = texte;
			span.id = "span"+document.querySelectorAll(".message").length;
			document.querySelector(".messages").appendChild(span);
			setTimeout(function (){
				document.querySelector(".messages").removeChild(document.querySelector("#"+span.id));
			}, time);
		}

		function emoticones(){
			document.querySelector(".container-avatar").innerHTML = "";

			for(var i=0;i<23;i++){
				var figure = document.createElement("figure");
				var img = document.createElement("img");
				img.src = "images/avatar/png/boy"+((i==0)?"":"-"+i)+".png";
				img.alt = "avatar-boy"+((i==0)?"":"-"+i);
				img.id = "boy"+((i==0)?"":"-"+i);
				img.setAttribute("class", "img-avatar");
				figure.setAttribute("onclick", "avatar('"+img.id+"')");
				figure.appendChild(img);
				document.querySelector(".container-avatar").appendChild(figure);
			}
			for(var i=0;i<27;i++){
				var figure = document.createElement("figure");
				var img = document.createElement("img");
				img.src = "images/avatar/png/girl"+((i==0)?"":"-"+i)+".png";
				img.alt = "avatar-girl"+((i==0)?"":"-"+i);
				img.id = "girl"+((i==0)?"":"-"+i);
				img.setAttribute("class", "img-avatar");
				figure.setAttribute("onclick", "avatar('"+img.id+"')");
				figure.appendChild(img);
				document.querySelector(".container-avatar").appendChild(figure);
			}
			emos = document.querySelector(".container-avatar").innerHTML;
		}
		//Avatar sélectionné
		function avatar(id){
			avatar_id = id;
			document.querySelector(".container-avatar").innerHTML = "";
			var figure = document.createElement("figure");
			var img = document.createElement("img");
			img.src = "images/avatar/png/"+id+".png";
			img.alt = "avatar-"+id;
			img.id = id;
			img.setAttribute("class", "img-avatar");
			figure.setAttribute("class", "choose");
			figure.setAttribute("onclick", "remove_avatar()");
			figure.appendChild(img);
			document.querySelector(".container-avatar").appendChild(figure);
		}
		//Retirer un avatar
		function remove_avatar(){
			avatar_id = "";
			document.querySelector(".container-avatar").innerHTML = emos;
		}
		//Changer de page
		document.querySelector(".loader-body img").style.display = "none";	
		function page(num, num2 = 0, num3 = 0){
			if(document.querySelector(".loader-body img"))
				document.querySelector(".loader-body img").style.display = "inline-block";	
			if(num == 3){
				if(num3 == 1){
					document.querySelector(".connexion-form").style.display = "none";
					document.querySelector(".inscription-form").style.display = "flex";
				}
				if(num3 == 2){
					document.querySelector(".connexion-form").style.display = "flex";
					document.querySelector(".inscription-form").style.display = "none";
				}
			}
			if(num == 7){
				//page 7, page des commetaires
				setInterval(commentaires,1000);
			}
			else if(num == 9){
				//page 9, page des scores
				//setInterval(scores,1000);
				scores();
			}
			//Arrete la musique si elle continue a jouer a la fin de la partie
			document.querySelector("#audio").onplaying = function(){
				if(pause == 1)
					document.querySelector("#audio").pause();
			}

			if(num2 == 0)
				num2 = num-1;
			document.querySelector(".page"+num2).style.animation = "dissipe 1s ease";	
			document.querySelector(".page"+num2).style.opacity = "0";	
			document.querySelector(".page"+num2).style.transform = "scale(0)";	
			setTimeout(function (){
				for(var i=0;i<document.querySelectorAll(".page").length;i++)
					document.querySelectorAll(".page")[i].style.display = "none";
				document.querySelector(".page"+num).style.display = "flex";
				document.querySelector(".page"+num).style.animation = "entree 1s ease";	
				document.querySelector(".page"+num2).style.opacity = "1";	
				document.querySelector(".page"+num).style.transform = "scale(1)";	
				setTimeout(()=>{		
					if(document.querySelector(".loader-body img"))
						document.querySelector(".loader-body img").style.display = "none";	
				}, 500);
			}, 1000);
	
		}

		function partie_directe(){
			//enregistrement dans la base de données
			var id = 0;
			/*
			if(getCookie("user") != null){
				username = JSON.parse(getCookie("user")).username;
				avatar_id = JSON.parse(getCookie("user")).avatar_id;
				message("Bienvenue "+username, "", 5000);
				miseajour(username, avatar_id);
				lancer();
				return;
			}
			*/
			$.post("php/users.php", function (data){
				id = parseInt(data);
				message("Nous créons votre partie...", "", 5000);
				username = "anonyme"+id;
				email=username+"@tchatat.fr";
				password = username;
				avatar_id = "girl-1";
				$.post("php/save.php", {username:username, email:email, password:password, avatar:avatar_id}, function (data){
					if(data){
						console.log(data);
						travail = 0;
						message("Ce nom d'utilisateur et/ou cette adresse email existe(nt) déjà ...", "erreur", 6000);
						page(3, 4, 1);
					}
					else{
						message("Vos informations ont été enregistrées avec succès", "", 5000);
						message("Bienvenue "+username, "", 5000);
						miseajour(username, avatar_id);
						lancer();
					}
				});
			});
			/*
			*/
		}

		function fullscreen(){
			/*Code pour mettre le plein écran lors du clic */
			document.addEventListener("click", function (){
				var docElm = document.documentElement;
				if (docElm.requestFullscreen) {
				    docElm.requestFullscreen();
				}
				else if (docElm.mozRequestFullScreen) {
				    docElm.mozRequestFullScreen();
				}
				else if (docElm.webkitRequestFullScreen) {
				    docElm.webkitRequestFullScreen();
				}
				else if (docElm.msRequestFullscreen) {
				    docElm.msRequestFullscreen();
				}
			});
		}