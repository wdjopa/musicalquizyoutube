var questions = [];

questions.push({
	"question": "Quel est le nom d'artiste de cette étoile montante ?",
	"reponse": "Olgha NK",
	"propositions": "Olgha NK,Sorelle,Cold,Calysto",
	"musique": "uOehyeEImHU",
	"debut": 54,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Quel est le titre de cette chanson de Mr Africa ft Sojip ?",
	"reponse": "Lettre aux Bâtisseurs",
	"propositions": "Lettre aux Bâtisseurs,Serment,Cameroon,Batissons le ensemble",
	"musique": "dqVSVlY_QNQ",
	"debut": 25,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Quel autre pseudonyme porte l'artiste Nyangono du Sud ?",
	"reponse": "Le lion du Sud",
	"propositions": "Le lion du Sud,Le pere des enfants,Foup-fap,Arrangemento",
	"musique": "IG55NBKx63A",
	"debut": 1,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Avec quel artiste Mr Leo a été en featuring dans le morceau 'Supporter' ?",
	"reponse": "Locko",
	"propositions": "Locko,Minks,Ko-C,Numerica",
	"musique": "4IZj_G7Oxiw",
	"debut": 18,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Quel est le titre de ce morceau de notre Blanche ?",
	"reponse": "Ton pied Mon pied",
	"propositions": "Ton pied Mon pied,Tu m'aimes comme ca,Mon amour,Kongossa",
	"musique": "TgxPoqAnik4",
	"debut": 18,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Qui n'était pas dans le remix de ce tube ?",
	"reponse": "Locko",
	"propositions": "Locko,Magasco,Mr Leo,Ko-C",
	"musique": "6LDLZbCngzM",
	"debut": 18,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Dans ce trio, nous avons Stanley Enow, Locko et ? ",
	"reponse": "Tzy Panchak",
	"propositions": "Tzy Panchak,Numerica,Mr Leo,Ko-C",
	"musique": "T46oVO0KyCA",
	"debut": 85,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Le titre est ? (ps : Ca tourne autour du piment 😁)",
	"reponse": "Pimentcam",
	"propositions": "Pimentcam,Piment,Pimenterie,Le piment",
	"musique": "YS3vTuHjAvQ",
	"debut": 15,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Avec qui Ko-c est-il en featuring ?",
	"reponse": "Tenor",
	"propositions": "Tenor,Mr Leo,Locko,Minks",
	"musique": "2MhIx8lsrm4",
	"debut": 12,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Quel est le titre de cette musique de Mimie ?",
	"reponse": "Je m'en fous",
	"propositions": "Je m'en fous,Les mauvais choix,Waka waka,Pala pala",
	"musique": "E9hBm8LeaA8",
	"debut": 0,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Quel est le vrai prénom de cette Slameuse Camerounaise ?",
	"reponse": "Dolly",
	"propositions": "Dolly,Lydol,Cassandra,Sonia",
	"musique": "i8kVlKOFyPc",
	"debut": 10,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "De qui s'agit-il dans ce chant enjaillant ?",
	"reponse": "FUL",
	"propositions": "FUL,Locko,Singuila,Sojip",
	"musique": "viiYdv9ai7k",
	"debut": 19,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Quel est le titre de cette musique de Maahlox ?",
	"reponse": "Tu as combien ?",
	"propositions": "Tu as combien ?,Connecte toi,Maahlox,Ca sort",
	"musique": "UyZbwHyoLMY",
	"debut": 10,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "De quelle nationalité est ce musicien ?",
	"reponse": "Camerounaise",
	"propositions": "Camerounaise,Ivoirienne,Togolaise,Sénégalaise",
	"musique": "Uu77P13E7Ig",
	"debut": 10,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Quelle est l\'expression favorite de cet artiste ?",
	"reponse": "Le fiang le way le yamo",
	"propositions": "Le fiang le way le yamo, Ramzi, Tenor, Do le dab",
	"musique": "YihmpQdTj54",
	"debut": 10,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Cette chanson a été chanté par quels artistes ?",
	"reponse": "Mink\'s et Blanche Bailly",
	"propositions": "Mink\'s et Blanche Bailly,Mink\'s et Mani Bella, Mink\'s et Tenor, Mink\'s et C. Dipanda",
	"musique": "H1lBkyLASX0",
	"debut": 10,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "En quelle année est sortie ce titre de Daphnée ?",
	"reponse": "2017",
	"propositions": "2017,2015,2016,2018",
	"musique": "rs0IChj82GE",
	"debut": 10,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Quelle est le titre de cette musique ?",
	"reponse": "C\'est la vie",
	"propositions": "C\'est la vie,Nouvelle naissance,Merci la vie,Ma vie",
	"musique": "OkSIjMqFh6I",
	"debut": 15,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Quel artiste a chanté Do le Dab ... ?",
	"reponse": "TENOR",
	"propositions": "TENOR,MAAHLOX,FRANKO,MINK S",
	"musique": "o1AV068nAEo",
	"debut": 20,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Quel artiste reveille nos doux souvenirs dans cette chanson?",
	"reponse": "Henri Dikongue",
	"propositions": "Henri Dikongue,Richard Bona,Manu Dibango,Sergeo Polo",
	"musique": "OkSIjMqFh6I",
	"debut": 14,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Quel grand oui grand joueur de foot retrouve t on dans cette chanson?",
	"reponse": "ETO'O FILS",
	"propositions": "ETO'O FILS,ROGER MILLA,ETO'O ET MILLA,DROGBA",
	"musique": "hK8SgoHgOOY",
	"debut": 72,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Quelle est la durée approximative de cette chanson?",
	"reponse": "05min59",
	"propositions": "05min59,05min02,04min07,05min30",
	"musique": "D7uAbA5OtPw",
	"debut": 10,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Quel est le titre de cette chanson d'Eboa Lotin?",
	"reponse": "Buna ba Kwedi",
	"propositions": "Buna ba Kwedi,Essèlè nika,Elimba dikalo,Muntula moto",
	"musique": "nJZNek3oQJM",
	"debut": 130,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Dans la salle de tournage du premier couplet, que voit-on au dessus des voitures ?",
	"reponse": "Régime de plantain",
	"propositions": "Régime de plantain,Haut-parleur,Contorsionniste,Tamtam et batterie",
	"musique": "64gAmbtiqus",
	"debut": 106,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Quelle est l'origine du rappeur en featuring avec Booba ?",
	"reponse": "Cameroun",
	"propositions": "Cameroun,Antilles,Cote d'ivoire,Congo",
	"musique": "IWftfp5cLCk",
	"debut": 120,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "De quel pays est originaire le chanteur Dosseh ?",
	"reponse": "Cameroun",
	"propositions": "Cameroun,Ghana,Nigéria,Mali",
	"musique": "NaR2l0JXQas",
	"debut": 70,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Qui a chanté ?",
	"reponse": "Daouda",
	"propositions": "Daouda,Femi Futi,Cesaria Evoria,Richard Bona",
	"musique": "Zlx6qzoMS9k",
	"debut": 10,
	"image": "images/",
	"commentaire": ""
});
questions.push({
	"question": "Dans quel collège a étudié la belle Sandrine Nanga ?",
	"reponse": "Collège de la Retraite",
	"propositions": "Collège de la Retraite,Collège Vogt,Collège Libermann,Lycée d'Ekorezok",
	"musique": "nE3RviX-Wpc",
	"debut": 10,
	"image": "images/",
	"commentaire": ""
});

	/*
questions.push({
	"question":"Avec quel rappeur Américain Dosseh fait le featuring dans ce morceau ?",
	"reponse":"Young Thug",
	"propositions":"Young Thug,Rick Ross,Eminem,Lil Wayne",
	"musique":"musique/Dosseh - Milliers deuros ft. Young Thug.mp3",
	"debut":13,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"De quel pays est originaire ce rappeur ?",
	"reponse":"Guinnée",
	"propositions":"Guinnée,Cameroun,Mali,Benin",
	"musique":"musique/MHD- la Puissance (part.7)  clip officiel.mp3",
	"debut":1,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Qeul est le titre de l’album dont ce morceau est issu ?",
	"reponse":"Futur",
	"propositions":"Futur,Turfu,Autopsie,Lunatic",
	"musique":"musique/Booba - Futur.mp3",
	"debut":8,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"En quelle année est sortie l’album dont ce morceau est issu ?",
	"reponse":"2017",
	"propositions":"2017,2016,2015,2011",
	"musique":"musique/Naza - La Débauche (Clip Officiel).mp3",
	"debut":1,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"De quel album est issu ce son ?",
	"reponse":"Incroyable",
	"propositions":"Incroyable,Mouillez le Maillot,Naza,MMM",
	"musique":"musique/Naza - MMM (Clip Officiel).mp3",
	"debut":8,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Dans quelle maison de disque ce groupe a t-il signé ?",
	"reponse":"Universal Music France",
	"propositions":"Universal Music France,Sony Music,Electrons Music,Mavin Records",
	"musique":"musique/Toofan Ft. Patoranking - MA GIRL (Official Video).mp3",
	"debut":1,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Quel est le nom du label de cet artiste ?",
	"reponse":"Star Boy Entertainment",
	"propositions":"Star Boy Entertainment,Aluda Records,ACT Music,Arion",
	"musique":"musique/WizKid - Come Closer ft. Drake.mp3",
	"debut":21,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"En quelle année Youssoupha a t-il sorti son album Négritude ?",
	"reponse":"2015",
	"propositions":"2015,2016,2017,2011",
	"musique":"musique/Youssoupha - Entourage - (Clip Officiel).mp3",
	"debut":1,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Quel groupe ayant participé à The Voice a chanté cette musique?",
	"reponse":"Arcadian",
	"propositions":"FréroDelavega,Arcadian,Les enfoirés,Cheat codes",
	"musique":"musique/Folie_arcadienne.mp3",
	"debut":8,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Quel est le titre du nouvel album de Dadju?",
	"reponse":"Gentleman 2.0",
	"propositions":"Gentleman 2.0,Indéfini,Volume 1,Subliminal ",
	"musique":"musique/DADJU_-_Plus_L_temps.mp3",
	"debut":8,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Completez cette phrase de la chanson mannequin : Je t’ai donné mon coeur…………... ",
	"reponse":"Et maintenant c’est la guerre",
	"propositions":"Tu as dit pas besoin,Et maintenant c’est la guerre,Et maintenant tu me laisses,Et j’ai dit que je t’aime",
	"musique":"musique/Fally_Ipupa_–_Mannequin.mp3",
	"debut":44,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Avec qui Fally a chanté mannequin?",
	"reponse":"Keblack et Naza",
	"propositions":"Abou Tall et Keblack,Naza et Abou Tall,MHD et Keblack,Keblack et Naza ",
	"musique":"musique/Fally_Ipupa_–_Mannequin.mp3",
	"debut":38,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Quel est l’artiste masculin francophone de l’annee 2017 (NRJ music awards)?",
	"reponse":"Soprano",
	"propositions":"Matt Pokora,Soprano,Dadju,Calogero",
	"musique":"musique/Soprano_la_colombe_et_le_corbeau.mp3",
	"debut":18,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Qui a été élu meilleur dj de l’annee 2017 (NRJ music awards)?",
	"reponse":"Kungs",
	"propositions":"David Guetta,Kungs,DJ Snake,Feder",
	"musique":"musique/Kungs_-_Dont_You_Know.mp3",
	"debut":20,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"En quelle année s’est il marié?",
	"reponse":"2017",
	"propositions":"2017,2016,2015,2014",
	"musique":"musique/DADJU_-_Gentleman_20.mp3",
	"debut":7,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Qui a été élu l’artiste féminin internationale de l’année 2017 (NRJ music awards)?",
	"reponse":"Selena Gomez",
	"propositions":"Katy Perry,Miley Cyrus,Shakira,Selena Gomez",
	"musique":"musique/Selena Gomez - .Revival.mp3",
	"debut":45,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Quelle est la chanson internationale de l’année 2017 (NRJ music awards) ?",
	"reponse":"Despacito(Luis Fonsi ft Daddy Yankee)",
	"propositions":"Despacito(Luis Fonsi ft Daddy Yankee),Attention(Charlie Puth),Shape of you(Ed Sheeran),I feel it coming(The Weeknd)",
	"musique":"musique/despacito-luis-fonsi-daddy-yankee.mp3",
	"debut":90,
	"image":"images/",
	"commentaire":"Impressionant !!!!!!!!!! Il a eu le plus de vues sur youtube (4,1 Milliards en Novembre 2017)"
});
questions.push({
	"question":"Elle a fait The Voice… Saurez vous la reconnaitre ?",
	"reponse":"Louane",
	"propositions":"Manon,Louane,Anne Sila,Hiba Tawaji",
	"musique":"musique/On était beau_LOUANE.mp3",
	"debut":8,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Il a repri les tubes de Claude Francois ...",
	"reponse":"Matt Pokora",
	"propositions":"Kendji Girac,Matt Pokora,Lilian Renaud,Maximilien",
	"musique":"musique/despacito-luis-fonsi-daddy-yankee.mp3",
	"debut":13,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Qui a chanté cette chanson ?",
	"reponse":"Ma liberté de penser",
	"propositions":"Garou,Florent Pagny,Calogero,Pascal Obispo",
	"musique":"musique/Ma liberté de penser.mp3",
	"debut":50,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Qui a chanté ce son des années 80 ?",
	"reponse":"Joe Dassin",
	"propositions":"Florent Pagny,Joe Dassin,Michel Sardou,Claude Francois",
	"musique":"musique/Joe_Dassin_-_Et_Si_Tu_NExistait_Pas.mp3",
	"debut":8,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Il a gagné The voice saison 4",
	"reponse":"Lilian Renaud",
	"propositions":"Lilian Renaud,Amir,Maximillien,Slimane",
	"musique":"musique/Lilian_Renaud_-_Pour_Ne_Plus_Avoir_Peur.mp3",
	"debut":12,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Magnifique chanson dédiée aux mères... Quel en est le titre ?",
	"reponse":"Les beaux yeux de la mama",
	"propositions":"Maman,Tes beaux yeux mama,Aux beaux yeux de la Mama,Les beaux yeux de la mama",
	"musique":"musique/Les yeux de la mama Kendji Girac.mp3",
	"debut":25,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Quel est le nom de cet artiste ?",
	"reponse":"Niska",
	"propositions":"Niska,PNL,MHD,ALL BLACK",
	"musique":"musique/niska-reseau.mp3",
	"debut":10,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"De quel album est ce titre de Niska (Salé) ?",
	"reponse":"Commando",
	"propositions":"Commando,Charo Life,Zifukoro,Life",
	"musique":"musique/niska-sale.mp3",
	"debut":12,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Cette musique a été chantée par le groupe BYME. Ce groupe est composé de combien d\'artistes musiciens ?",
	"reponse":"6",
	"propositions":"4,5,6,7",
	"musique":"musique/bmye_pourquoi_cherie_ft._naza_keblack_youssoupha_hiro_jaymax_dj_myst_mp3_69392.mp3",
	"debut":15,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Quel artiste chante cette musique ?",
	"reponse":"Runtown",
	"propositions":"Runtown,Fanicko,Davido,Flavour",
	"musique":"musique/for_life_official_music_video_runtown_afrobeats_2017_mp3_37875.mp3",
	"debut":10,
	"image":"images/",
	"commentaire":""
});
questions.push({
"question":"Lequel des frères Jonas est l'auteur de cette chanson Introducing Me ?",
"reponse":"Nick Jonas",
"propositions":"Nick Jonas,Joe Jonas,Kevin Jonas,Maxime Jonas",
"musique":"musique/Introducing_Me.mp3",
"debut":35,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Quelle artiste internationale a repris cette musique de Francis Cabrel?",
"reponse":"Shakira",
"propositions":"Shakira,Beyonce,Tal,Amel Bent",
"musique":"musique/FRANCIS CABREL.mp3",
"debut":2,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Quelle compétition européenne a pour générique ceci?",
"reponse":"UEFA Champions league",
"propositions":"Champions league,UEFA Champions league,Major soccer league,le top 14",
"musique":"musique/UEFA_Champions_league_theme_song.mp3",
"debut":2,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Quel est le titre de cette musique de Toofan?",
"reponse":"Téré Téré",
"propositions":"Répé Répé,Péré péré,Téré Téré,Coller coller",
"musique":"musique/Toofan Téré Téré.mp3",
"debut":1,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Pour quelle compétition de foot a t on composé cette chanson?",
"reponse":"Coupe du monde de football 2014",
"propositions":"Coupe du monde de football 2014,Jeux olympiques de Rio 2016,Can 2015,Euro de football 2016",
"musique":"musique/We are one ole ola.mp3",
"debut":12,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Quel membre du groupe sexion d'assaut dit dans cette chanson la phrase suivante: c est la seule personne qui prie pour quitter ce monde avant toi",
"reponse":"LEFA",
"propositions":"Maitre Gims,Black M,LEFA,Maska",
"musique":"musique/Avant qu'elle parte.mp3",
"debut":86,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Quel est le titre de cette chanson de justin bieber où l'on retrouve des filles sexys se déhancher?",
"reponse":"Sorry",
"propositions":"Sorry,Fiesta,what do you mean,so sorry",
"musique":"musique/Justin Bieber-Sorry.mp3",
"debut":45,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Quelle est la dernière lettre de ce son qui vous a ému?",
"reponse":"n",
"propositions":"n,e,s,r",
"musique":"musique/See you again.mp3",
"debut":80,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Dans cette musique de Ed Sheeran que signifie en français le titre: thinking out loud?",
"reponse":"penser tout haut",
"propositions":"pensant tout haut,penser tout haut,pensant plus bas,penser vraiment haut",
"musique":"musique/Ed sheeran.mp3",
"debut":1,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"cet afro trap de MHD est le numéro...",
"reponse":"5",
"propositions":"5,4,3,6",
"musique":"musique/Ngatie abedi.mp3",
"debut":10,
"image":"images/",
"commentaire":""
});
questions.push({
	"question":"Que signifie en francais veni vidi vici?",
	"reponse":"je suis venu j ai vu j ai vaincu",
	"propositions":"je suis venu j ai vu j ai vaincu,je suis venu j ai vu j ai cru,je suis venu j ai vu j ai faxé,ça ne veut rien dire",
	"musique":"musique/veni vidi vici.mp3",
	"debut":10,
	"image":"images/",
	"commentaire":""
});
questions.push({
"question":"cette chanson de stromae figure dans lequel de ses albums?",
"reponse":"racine carrée",
"propositions":"racine carrée,racine cubique,cube magique,argument sinus hyperbolique",
"musique":"musique/Papaoutai.mp3",
"debut":15,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Quel nom donne t on a ce groupe qui a fait vibrer nos tantes (les miennes en tout cas)?",
"reponse":"Westlife",
"propositions":"Westlife,Beatles,East life,Prepa life",
"musique":"musique/Westlife Unbreakable.mp3",
"debut":2,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Avec quel artiste Singuila pleurniche dans cette chanson?",
"reponse":"Marc Antoine",
"propositions":"Marc Antoine,Martinez,Wilfried,M Pokora",
"musique":"musique/Singuila Marc Antoine Je suis KO.mp3",
"debut":20,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Quels sont les dj qu on retrouve dans l'original de ce remix des Pentatonix ?",
"reponse":"Dj snake et Major Lazer",
"propositions":"Dj snake et Major Lazer,Dj snake et David Guetta,David Guetta et Major Lazer,Arafat dj et dj mix",
"musique":"musique/lean on.mp3",
"debut":25,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Où Booba a-t-il tourné le clip de cette chanson ?",
"reponse":"Dakar",
"propositions":"Dakar,Yaoundé,Boulogne,Abidjan",
"musique":"musique/booba-dkr.mp3",
"debut":106,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Combien de lettres comportent le nom de l'auteur de cette chanson ?",
"reponse":"9",
"propositions":"7,8,9,10",
"musique":"musique/panda.mp3",
"debut":36,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Quels sont les grands joueurs dont parle Soprano dans cosmo?",
"reponse":"Messi et C.Ronaldo",
"propositions":"Messi et C.Ronaldo,Zlatan et Messsi,Suarez et Neymar,Buffon et Bassogog",
"musique":"musique/cosmo.mp3",
"debut":10,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Lequel des membres de sexion d'assaut est absent dans le clip de cette chanson?",
"reponse":"Aucun des 03",
"propositions":"Aucun des 03,LEFA,Maska,BLACK M",
"musique":"musique/J'reste Debout.mp3",
"debut":29,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Quel est le titre de ce générique phénoménal?",
"reponse":"We gotta power",
"propositions":"We gotta power,we got the power,Dragon ball z,we gotta a power",
"musique":"musique/We gotta power.mp3",
"debut":0,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Assiah les gars cette question est plus fille mais dites moi seulement avec qui violetta chante dans cette musique?",
"reponse":"Leon",
"propositions":"Leon,Santiago,Thomas,Maxi",
"musique":"musique/voy por ti.mp3",
"debut":10,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Quelle est la couleur de l'oiseau dans le clip de Carmen ?",
"reponse":"Bleu-ciel",
"propositions":"Bleu-ciel,Rouge,Vert,Bleu Indigo electrique",
"musique":"musique/Stromae_-_carmen.mp3",
"debut":25,
"image":"images/",
"commentaire":""
});
questions.push({
"question":" De quel instrument Sidiki joue t-il dans ce son?",
"reponse":"La kora",
"propositions":"La kora,Le djembé,La djebara,Le banjo",
"musique":"musique/Sidiki Diabaté - Dakan Tigui remix (Clip Officiel 2).mp3",
"debut":120,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Qui a chanté la version originale ?",
"reponse":"Mario",
"propositions":"Mario,Bracket,PSquare,Myriam Makeba",
"musique":"musique/Bracket - Nana _Official Video_.mp3",
"debut":118,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Qui a chanté la version originale de <b>éloko oyo</b>?",
"reponse":"Mabele Elisi",
"propositions":"Mabele Elisi,Fally Ipupa,Papa Wemba,Awilo Longomba",
"musique":"musique/Fally Ipupa - Eloko Oyo (Clip officiel).mp3",
"debut":90,
"image":"images/",
"commentaire":""
});
questions.push({
"question":"Un autre titre(hormis celui ci) du même auteur est: ?",
"reponse":"Bobo",
"propositions":"Bobo,Duro,Wo,Girlie O",
"musique":"musique/Olamide - Wo!!.mp3",
"debut":106,
"image":"images/",
"commentaire":""
});


questions.push({
"question":"Qui dit <b>Pour venir manger y'a pas besoin de programmer</b> ?",
"reponse":"Mamadou",
"propositions":"Mamadou,Black M,Barack Adama,Maitre Gims",
"musique":"musique/SEXION DASSAUT - AFRICAIN (CLIP OFFICIEL).mp3",
"debut":175,
"image":"images/",
"commentaire":""
});


questions.push({
	"question":"Quel est le titre de cette chanson de Booba?",
	"reponse":"Comme une étoile",
	"propositions":"La vie n’est qu’une escale,Comme une étoile,Futur,Caramel",
	"musique":"musique/Booba - Comme Une Etoile.mp3",
	"debut":10,
	"image":"images/",
	"commentaire":""
});

questions.push({
	"question":"Quel est le nom groupe présent parmi les artistes de cette chanson",
	"reponse":"Clean Bandit",
	"propositions":"Daft Punk,Clean Bandit,Tokyo Hotel,Damso",
	"musique":"musique/Rockabye Baby  Clean Bandit ft  Sean Paul   Anne Marie Lyrics.mp3",
	"debut":10,
	"image":"images/",
	"commentaire":""
});

questions.push({
	"question":"Qui est le chanteur intervenant dans cette chanson?",
	"reponse":"Coldplay",
	"propositions":"Coldplay,Justin Bieber,Thomas Ngijol,Zayn",
	"musique":"musique/The Chainsmokers  Coldplay - Something Just Like This LYRICS!.mp3",
	"debut":10,
	"image":"images/",
	"commentaire":""
});


questions.push({
	"question":"Ce générique est présent dans quel animé (ou Mangas) ?",
	"reponse":"Naruto",
	"propositions":"Naruto,One Piece,Reborn,Saiyuki",
	"musique":"musique/Full_opening_naruto_Blue_Bird.mp3",
	"debut":8,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"Dans quel animé trouve t-on cette bande sonore ?",
	"reponse":"Naruto",
	"propositions":"Naruto,Dragon Ball,Nicky Larson,Ranma",
	"musique":"musique/Naruto Shippuden - Girei (Pain's Theme Song).mp3",
	"debut":8,
	"image":"images/",
	"commentaire":""
});
questions.push({
	"question":"De quel animé vient ce générique ?",
	"reponse":"Nicky Larson",
	"propositions":"Nicky Larson,Le petit Lord,Saiyuki,Détective Conan",
	"musique":"musique/City Hunter - Get Wild.mp3",
	"debut":1,
	"image":"images/",
	"commentaire":""
});

questions.push({
	"question":"Avec qui Fally a fait le featuring pour ce morceau ?",
	"reponse":"Booba",
	"propositions":"Booba,Kaaris,MHD,Youssoupha",
	"musique":"musique/Fally Ipupa feat. Booba - Kiname (Clip officiel).mp3",
	"debut":43,
	"image":"images/",
	"commentaire":""
});

questions.push({
	"question":"Quel est le titre de cette musique ?",
	"reponse":"Kiname",
	"propositions":"Kiname,Tout Paname,Eloko oyo,Paname",
	"musique":"musique/Fally Ipupa feat. Booba - Kiname (Clip officiel).mp3",
	"debut":43,
	"image":"images/",
	"commentaire":""
});

questions.push({
	"question":"Ce morceau de Aya Nakamura figure dans lequel de ses albums ?",
	"reponse":"Journal Intime",
	"propositions":"Journal Intime,Comportement,Aya Nakamura,Ambiance",
	"musique":"musique/Aya Nakamura - Comportement (Clip officiel).mp3",
	"debut":28,
	"image":"images/",
	"commentaire":""
});
*/
