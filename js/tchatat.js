	function limiteTexte(text, size) {
	    if (text.length > size)
	        return text.substr(0, size)+' ...';
	    return text;
	}
	 function isEmail(email){
		var regEmail = new RegExp('^[0-9a-z._-]+@{1}[0-9a-z.-]{2,}[.]{1}[a-z]{2,5}$','i');
		return regEmail.test(email);
	   }
	function c(str){
		var chaine = "";
		var carac="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789àâéêèëîïôöçù',!?.;:=+-()/@%$ *";
		var tab = carac.split("");
		for(var i=0;i<str.length;i++){
			if(in_array(str[i], tab)){
				chaine+=carac[(position(str[i], tab) + 1) % carac.length];
			}
			else{
				chaine+=str[i];	
			}
		}
		return chaine;
	}
	// Ancienne chaine : abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789àâéêèëîïôöçù',!?.;:=+-()/@%$ *
	function d(str){
		var chaine = "";
		var carac="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789àâéêèëîïôöçù',!?.;:=+-()/@%$ *";
		var tab = carac.split("");
		for(var i=0;i<str.length;i++){
			if(in_array(str[i], tab)){
				chaine+=carac[(position(str[i], tab) - 1) % carac.length];
			}
			else{
				chaine+=str[i];	
			}
		}
		return chaine;
	}

	function shuffle(ch){

		var ch1 = comma(ch), ch2 = "";

		for(i=0;i<ch.length;i++){

			var a=random(0, ch1.length);

			ch2+=ch1[a];

			supprbypos(a, ch1);

		}

		return ch2;

	}

	function comma(ch2){

		var ch = [];

		for(var i=0;i<ch2.length;i++){

			ch[i]=ch2[i];

		}

		return ch;

	}

	function without(ch2){

		var ch = "";

		for(var i=0;i<ch2.length;i++){

			ch+=ch2[i];

		}

		return ch;

	}

	function take(num, tab){

		var ch = "";

		for(var i = 1;i<=num;i++){

			ch+=tab[random(0, tab.length)];

		}

		return ch;

	}

	function random(min, max){

		if(max <= min)

			return min;

		else

			return (parseInt( Math.random() * 1000 ) + min) % (max - min) + min;

	}

	function in_array(elt, tab){

		var i, a=0;

		for(i=0 ; i<tab.length ; i++)

			if(tab[i] == elt)

			{

				a = 1;

				break;

			}

		return a;

	}

	function del(elt, tab){

		var i, a=-1;

		for(i=tab.length-1 ; i>=0 ; i--)

			if(tab[i] == elt)

				a = i;

		clearTimeout(tab[a]);

		supprbypos(a, tab);

		console.log(tab);

	}

	function position(elt, tab){

		var i, a=-1;

		for(i=tab.length-1 ; i>=0 ; i--)

			if(tab[i] == elt)

			{

				a = i;

			}

		return a;

	}

	function suppr(elt, tab){

		var i, a=-1;

		for(i=tab.length-1 ; i>=0 ; i--)

			if(tab[i] == elt)

			{

				a = i;

			}

		if(a != -1){				

			for(i=a ; i<tab.length ; i++){

				tab[i] = tab[i+1];

			}

			tab.pop();

		}

	}

	function supprbypos(a, tab){

		var i;

		for(i=a ; i<tab.length ; i++){

			tab[i] = tab[i+1];

		}

		tab.pop();

	}

    function melange(elt){

        

            var longueur = elt.length, i = 0, nouveau = [];

            nombre = parseInt((Math.random().toFixed(1) * longueur));

            while(elt[i]){

                while(nouveau[nombre] || nombre < 0|| nombre > longueur-1){

                    nombre = parseInt((Math.random().toFixed(1) * longueur));

                }

                nouveau[nombre] = elt[i];

                i++;

            }

            return nouveau;

	}
	
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function deleteCookie(cname){
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}
function checkCookie() {
    var user = getCookie("username");
    if (user != "") {
        alert("Welcome again " + user);
    } else {
        user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie("username", user, 365);
        }
    }
}