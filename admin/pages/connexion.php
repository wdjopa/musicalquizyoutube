<div class="justify-content-center d-flex">
    <div class="card mt-4  mb-4 col-sm-12 col-md-6 p-0">
        <div class="card-header bg-primary text-white w-100">
            Connexion
        </div>
        <div class="card-body">
            <form class="form" onsubmit="connexion()">
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" id="username" aria-describedby="username" placeholder="Username" autofocus>
                <small id="emailHelp" class="form-text text-muted text-danger">Le même que celui utilisé pour jouer</small>
            </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="Password">
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Se souvenir de moi</label>
                </div>
                <button type="button" onclick="connexion()" class="btn btn-primary">Connexion</button>
            </form>
        </div>
    </div>
</div>

<script>
    

    function message(texte = "Erreur", type = "normal", time = 3000){
        var span = document.createElement("span");
        span.setAttribute("class", "message "+type);
        if(type == "info")
            span.innerHTML = "<span class='circle'>i</span> "+texte;
        else
            span.innerHTML = texte;
        span.id = "span"+document.querySelectorAll(".message").length;
        document.querySelector(".messages").appendChild(span);
        setTimeout(function (){
            document.querySelector(".messages").removeChild(document.querySelector("#"+span.id));
        }, time);
    }


    function connexion(){
        var username = document.querySelector("#username").value.trim();
        var password = document.querySelector("#password").value;
        if(username.length > 0 && password.length > 0){
            message("Connexion en cours ... Veuillez patienter");
            $.post("../php/connexion-form.php", {username:username, password:password}, function (data){
                if(data == "erreur_user"){
                    message("Le nom d'utilisateur n'est pas valide.", "erreur", 6000);
                    setTimeout(message, 3000, "Si vous ne vous êtes jamais inscrits, inscrivez-vous !", "info", 5000);
                    travail = 0;
                }
                else if(data == "erreur_pass"){
                    message("Le mot de passe n'est pas valide.", "erreur", 6000);
                    setTimeout(message, 3000, "Si vous ne vous avez oublié le mot de passe <button onclick=forgottenpassword('ouvrir')>Cliquez ici</button>!", "info", 5000);
                    travail = 0;
                }
                else{
                    setCookie("user_admin",JSON.stringify({"username":data.split("**")[0],"avatar":data.split("**")[1]}),3);
                    console.log(data);
                    message("Connexion établie");
                    message("Données de connexion récupérées ...");
                    setTimeout(() => {         
                        let d = encodeURI("./index.php?user="+username+"&page=admin");  
                        window.location.href=d;
                    }, 1000);
                }
            });
        }
        else{
            travail = 0;		
            if(password.length<=0){
                message("Vous n'avez pas entré de mot de passe ... ", "erreur", 6000);
                password.focus();
            }
            else{	
                message("Vous n'avez pas entré un nom d'utilisateur ... ", "erreur", 6000);
                username.focus();
            }
        }
    }	
</script>