<?php
    include "../../php/fonctions.php";
    if(!isset($_SESSION['user'])){
        header("Location: ../index.php");
    }
    else if(!isset($_GET['id'])){
        header("Location: ../index.php?page=admin&action=questions");
    }
    else{
        $id = htmlentities($_GET['id']);            
        $rep = $db -> prepare("DELETE FROM quiz_questions WHERE id=?");
        $rep -> execute(array($id));
        header("Location: ../index.php?page=admin&action=questions");
    }

?>