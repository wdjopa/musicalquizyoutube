<?php
    include "../../php/fonctions.php";
    var_export($_FILES);
    $id = htmlentities($_POST['id']);
    $question = htmlentities($_POST['question']);
    $reponse = htmlentities($_POST['reponse']);
    $proposition1 = htmlentities($_POST['proposition1']);
    $proposition2 = htmlentities($_POST['proposition2']);
    $proposition3 = htmlentities($_POST['proposition3']);
    $debut = htmlentities($_POST['debut']);
    $youtube = htmlentities($_POST['lien']);

    $target_dir = "musique/";
    $musique = $target_dir.basename($_FILES["fichier"]["name"]);
    if($_FILES["fichier"]["name"] == ""){
        $req = $db -> prepare("UPDATE quiz_questions SET question=?,reponse=?,proposition1=?,proposition2=?,proposition3=?,debut=?, musique=?, youtube=? WHERE id=?");
        $req -> execute(array($question,$reponse,$proposition1,$proposition2,$proposition3,$debut,$musique,$youtube,$id)); 
        header("Location: ../?page=admin&action=questions&success");
    }
    else{
        if(move_uploaded_file($_FILES["fichier"]["tmp_name"], "../../".$musique)){
            $req = $db -> prepare("UPDATE quiz_questions SET question=?,reponse=?,proposition1=?,proposition2=?,proposition3=?,debut=?,musique=?, youtube=? WHERE id=?");
            $req -> execute(array($question,$reponse,$proposition1,$proposition2,$proposition3,$debut,$musique,$youtube,$id)); 
            header("Location: ../?page=admin&action=questions&success");
        }
        else{
            header("Location: ../?page=admin&action=questions&echec");
        }
    }
    
    
?>