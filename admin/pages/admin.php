<?php
    if(isset($_GET['user'])){
        $_SESSION['user']=htmlentities($_GET['user']);
        header("Location: ?page=admin");
    }
    else if(!isset($_SESSION['user'])){
       header("Location: ?page=connexion");
    }
?>
<style>
    .logo{
        width: 10rem;
        padding: 2.5rem;
    }
    img{
        width: 100%;
    }
    .logo img{
        border-radius: 100rem;
    }
    a{
        cursor: pointer;
    }
    audio{
        width: 100%;
    }
    .list-card .row{
        text-align: center;
        display: flex;
        justify-content:center;
    }
    .list-card .btn{
        width: 40%;
        overflow: hidden;
        word-wrap: break-word;
        text-overflow: ellipsis;
        white-space: nowrap;
    }
</style>





<div class="d-flex mt-4 align-items-center">
    <div class="left mr-4">
        <div class="text-center ">
            <div class="logo">
                <img alt="avatar" class="avatar">
            </div>
            
        </div>    
    </div>
    <div class="center">
        <h5>Salut <span class="username"></span>, Merci de bien vouloir participer à cette aventure 🙂</h5>
        <div class="dropdown-divider"></div>
        <small>PS : Nous souhaitons promouvoir la musique camerounaise, nous serons ravis que tu ajoutes des questions en rapport avec ces artistes 🔥.</small>
    </div>
</div>
<div class="buttons text-white mt-5 mb-5">
    <button class="btn w-100 btn-warning"data-toggle="modal" data-target="#trouverId">Comment trouver l'id de la video ?</button>
    <div class="d-flex justify-content-center align-items-center h-100">
        <a class="btn mt-3 btn-primary btn" href="?page=admin&action=add">Ajouter une question</a>
        <a class="btn ml-3 mr-3 mt-3 btn-success btn" href="?page=admin&action=questions">Mes questions (<span class="total-questions"></span>)</a>
        <a class="btn mt-3 btn-danger btn" href="../">Retourner jouer</a>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="trouverId" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Comment trouver l'id d'une video</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Si vous êtes sur Smartphone</h5>
        <ol>
            <li>Allez sur YouTube, trouvez la musique que vous souhaitez faire jouer en fond</li>
            <li>Cliquez sur le bouton <span class="text-success">Partager</span>
                <br>
                <img src="images/yt.jpg" alt="">
            </li>
            <br>
            <li>Puis <span class="text-success">Copier dans le presse papier</span>
                <br>
                <img src="images/partager.jpg" alt="">
            </li>
            <li>Le résultat sera un lien comme ceci : <a href="https://lamater.net">https://youtu.be/i8kVlKOFyPc</a></li>
            <br>
            <h4>L'Id est donc la partie rouge https://youtu.be/<span class="text-danger">i8kVlKOFyPc</span></h4>
        </ol> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<?php
if(isset($_GET['action']) && htmlentities($_GET['action']) == "add"):
?>

<div class="form card mt-3 mb-4">
    <div class="card-header text-white bg-primary">
        Nouvelle question
    </div>
    <form class="p-3"enctype="multipart/form-data" action="pages/upload.php" name="form" method="post">
    <small>Les champs ayant la mention (<span class="text-danger">*</span>) sont <u>obligatoires</u>.</small>
    <br>
    <br>
        
    <div class="form-group">
        <label for="question"><span class="text-danger">*</span>Intitulé de la question</label>
        <input type="hidden" class="form-control" name="user">
        <input type="text" class="form-control" name="question" placeholder="Intitulé de la question" required>
    </div>
    <div class="form-group">    
        <label for="lien"><span class="text-danger">*</span>Id de la video YouTube - Titre : <span class="h6 titre-video"></span></label>
        <input type="text" class="form-control" name="lien" placeholder="Id de la vidéo YouTube (par ex: F1vS9b8lVV8)" required>
        <small>Pour le lien : <a href="https://www.youtube.com/watch?v=F1vS9b8lVV8&start_radio=1&list=RDF1vS9b8lVV8">https://www.youtube.com/watch?v=<u>F1vS9b8lVV8</u>&start_radio=1&list=RDF1vS9b8lVV8</a>, Le code est celui qui se trouve après le <b>?v=</b>. Soit <span class="text-danger">F1vS9b8lVV8</span></small>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="reponse"><span class="text-danger">*</span>Réponse</label>
            <input type="text" class="form-control" name="reponse" placeholder="La Réponse" required>
        </div>
        <div class="form-group col-md-6">
            <label for="proposition1"><span class="text-danger">*</span>Proposition 1</label>
            <input type="text" class="form-control" name="proposition1" placeholder="Proposition N°1" required>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="proposition2"><span class="text-danger">*</span>Proposition 2</label>
            <input type="text" class="form-control" name="proposition2" placeholder="Proposition N°2" required>
        </div>
        <div class="form-group col-md-6">
            <label for="proposition3"><span class="text-danger">*</span>Proposition 3</label>
            <input type="text" class="form-control" name="proposition3" placeholder="Proposition N°3" required>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="debut"><span class="text-danger">*</span>Debut du morceau </label>
            <input type="number" min="0" class="form-control" name="debut" placeholder="Entrez par ex : 60 'pour que la musique commence à la 1ere minute'" required>
        </div>
        <div class="form-group col-md-6">
            <label for="debut">Uploadez votre fichier audio (<span class="text-secondary">optionnel</span>)</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="morceau">Morceau</span>
                </div>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="fichier" id="file" aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label file" for="file">Uploadez</label>
                </div>
            </div>
        </div>
    </div>
    <br>
<!--
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="categorie">Catégorie</label>
            <select id="categorie" class="form-control" required>
                <option selected>Sélectionnez une catégorie ...</option>
                <option value="rap">Rap</option>
                <option value="rnb">Rap</option>
                <option value="benskin">Benskin</option>
                <option value="makossa">Makossa</option>
                <option value="makossa">Bikutsi</option>
                <option value="pop">Pop</option>
                <option value="afropop">Afropop</option>
            </select>
        </div>
    </div>
-->  
    <button type="submit" name="submit" class="btn btn-primary">Enregistrer</button>
    </form>
</div>

<?php
else: 
?>
<div class="all-questions d-flex justify-content-around" style="flex-wrap:wrap;"></div>

<!-- Modal Supprimer-->
<div class="modal fade" id="supprimerQuestion" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Suppression de question</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Êtes-vous sûr de vouloir supprimer cette question ?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Non</button>
        <a class="btn btn-danger text-white modal-delete">Oui</a>
      </div>
    </div>
  </div>
</div>
<!-- Modal Modifier -->
<div class="modal fade" id="modifierQuestion" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Modification de question</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="p-3"enctype="multipart/form-data" action="pages/uploadModifier.php" name="formModifier" method="post">
                    <small>Les champs ayant la mention (<span class="text-danger">*</span>) sont <u>obligatoires</u>.</small>
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="question"><span class="text-danger">*</span>Intitulé de la question</label>
                        <input type="hidden" class="form-control" name="id">
                        <input type="text" class="form-control" name="question" placeholder="Intitulé de la question" required>
                    </div>
                    <div class="form-group">    
                        <label for="lien"><span class="text-danger">*</span>Id de la video YouTube - Titre : <span class="h6 titre-video"></span></label>
                        <input type="text" class="form-control" name="lien" placeholder="Id de la vidéo YouTube (par ex: F1vS9b8lVV8)" required>
                        <small>Pour le lien : <a href="https://www.youtube.com/watch?v=F1vS9b8lVV8&start_radio=1&list=RDF1vS9b8lVV8">https://www.youtube.com/watch?v=<u>F1vS9b8lVV8</u>&start_radio=1&list=RDF1vS9b8lVV8</a>, Le code est celui qui se trouve après le <b>?v=</b>. Soit <span class="text-danger">F1vS9b8lVV8</span></small>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="reponse"><span class="text-danger">*</span>Réponse</label>
                            <input type="text" class="form-control" name="reponse" placeholder="La Réponse" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="proposition1"><span class="text-danger">*</span>Proposition 1</label>
                            <input type="text" class="form-control" name="proposition1" placeholder="Proposition N°1" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="proposition2"><span class="text-danger">*</span>Proposition 2</label>
                            <input type="text" class="form-control" name="proposition2" placeholder="Proposition N°2" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="proposition3"><span class="text-danger">*</span>Proposition 3</label>
                            <input type="text" class="form-control" name="proposition3" placeholder="Proposition N°3" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="debut"><span class="text-danger">*</span>Debut du morceau </label>
                            <input type="number" min="0" class="form-control" name="debut" placeholder="Entrez par ex : 60 'pour que la musique commence à la 1ere minute'" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="debut">Uploadez votre fichier audio</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="morceau">Morceau</span>
                                </div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="fichier" id="file" aria-describedby="inputGroupFileAddon01">
                                    <label class="custom-file-label filename" for="file"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                        <button type="submit" name="submit" class="btn btn-primary">Modifier</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?php
endif;
?>



<div id="background-display"></div>
<script src="../js/yout.js"></script>
<script>
    
    
    var avatar, pseudo, user, ques=[];
    let vid = null;
    var limite = 1;
    
    if(!getCookie("user_admin")){
        document.location.href="./?page=connexion";
    }
    else{
        user = JSON.parse(getCookie("user_admin"));
        username = user.username;
        avatar = user.avatar;
        document.querySelector(".avatar").src="../images/avatar/png/"+avatar+".png";
        document.querySelector(".username").textContent = username;
        if(document.form)
        document.form.user.value = username;
    }

    if(document.form){
        
        document.querySelector("#file").addEventListener('change', ()=>{
            $(".file").text(document.querySelector("#file").files[0].name);
        });
        document.form.lien.addEventListener('change', ()=>{
            console.log("sdf");
            if(vid){
                vid.destroy();
            }
            vid = new YT.Player('background-display', {
                height: '1',
                width: '1',
                videoId: document.form.lien.value ,
                events: {
                    'onReady': function (){
                        if(!vid.getVideoData().title)
                        $(".titre-video").html("<span class='text-danger'>Aucune video ne correspond à cet ID</span>")
                        else
                            $(".titre-video").html("<span class='text-success'>"+vid.getVideoData().title+"</span>")
                        }
                    }
                })
            });
    }
    if(document.formModifier){
        document.formModifier.addEventListener('change', ()=>{
            $(".filename").text(document.formModifier.fichier.files[0].name);
        });
    }
    chargerSuite();

    function chargerSuite(page=1){
        $.post("pages/questions.php", {username:username}, function (data){
            $(".total-questions").text(data);
        })
        $.post("pages/questions.php", {username:username, limit:page}, function (data){
            questions = JSON.parse(data);
            if(data != ""){
                questions.forEach(question => {
                    $(".all-questions").append(
                    '<div class="card list-card m-4" style="width: 30rem;"><iframe style="width:100%;" class="card-img-top" src="https://www.youtube.com/embed/'+question.musique+'?start='+question.debut+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><div class="card-body"><h5 class="card-title">Question : '+question.question+'</h5><h6 class="card-subtitle mb-2 text-muted">Reponse : '+question.reponse+'</h6><p class="card-text"><div class="row"><div class="m-2 btn btn-primary">'+question.proposition1+'</div><div class="m-2 btn btn-primary">'+question.proposition2+'</div></div><div class="row"><div class="m-2 btn btn-primary">'+question.proposition3+'</div><div class="m-2 btn btn-success">'+question.reponse+'</div></div><br></p><a  data-toggle="modal" data-target="#modifierQuestion" onclick="remplir('+question.id+')" class="card-link text-success">Modifier</a><a data-toggle="modal" data-target="#supprimerQuestion" onclick="supp('+question.id+')" class="card-link text-danger">Supprimer</a></div></div>')
                    ques[question.id] = question;
                });
            }
        });
    }
    $(window).on("scroll", function() {
        var scrollHeight = $(document).height();
        var scrollPosition = $(window).height() + $(window).scrollTop();
        if ((scrollHeight - scrollPosition) < 20) {
            // when scroll to bottom of the page
            chargerSuite(++limite);
        }
    });
    function supp(id){
        document.querySelector("a.modal-delete").setAttribute("href","./pages/supprimer.php?id="+id);
    }
    function remplir(a){
        document.formModifier.id.value = a;
        document.formModifier.question.value = ques[a].question;
        document.formModifier.reponse.value = ques[a].reponse;
        document.formModifier.proposition1.value = ques[a].reponse;
        document.formModifier.proposition2.value = ques[a].proposition2;
        document.formModifier.proposition3.value = ques[a].proposition3;
        document.formModifier.debut.value = ques[a].debut;
        document.formModifier.lien.value = ques[a].musique;
    }
</script>
